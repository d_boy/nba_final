# from __future__ import print_function
# from nba_py import game
# import psycopg2
# import pandas
# from util import *
# from db_functions import *
# from betting import *

# def parse_fantasy_file_1415(line):

# 	line_arr = line.split(':')

# 	if (str(line_arr[0]) == "GID"):
# 		print("done!")
# 		return

# 	fantasy_player_id = line_arr[0]
# 	full_name = line_arr[1].split(", ")

# 	last_name = full_name[0]
# 	last_name = last_name.replace("'", "")
# 	last_name = last_name.replace(".", "")
# 	last_name = last_name.replace("-", "")
# 	first_name = full_name[1]
# 	first_name = first_name.replace("'", "")
# 	first_name = first_name.replace(".", "")
# 	first_name = first_name.replace("-", "")
# 	table_ref = "%s_%s" % (str(first_name), str(last_name))

# 	if (last_name == "Hilario"):
# 		table_ref = "nene_"
	
# 	player_id = assign_player_id(first_name, last_name)
# 	game_date = line_arr[2]
# 	team = line_arr[3]
# 	opp_team = line_arr[4]
# 	home_or_away = line_arr[5]
# 	starter = getInt(line_arr[6])
# 	minutes = getDouble(line_arr[7])
	
# 	if home_or_away == "A":
# 		away_team = team
# 		home_team = opp_team
# 	else:
# 		away_team = opp_team
# 		home_team = team

# 	game_id = assign_game_id_fantasy(away_team, home_team, game_date)
# 	appeared = getInt(line_arr[8])
# 	line_score = line_arr[18] 
# 	draftkings_pts = getDouble(line_arr[15])
# 	fanduel_pts = getDouble(line_arr[10]) 
# 	draftkings_salary = getInt(line_arr[21])
# 	fanduel_salary = getInt(line_arr[19])
# 	draftkings_value = calc_value(draftkings_salary, draftkings_pts)
# 	fanduel_value = calc_value(fanduel_salary, fanduel_pts)
# 	draftkings_pos = line_arr[26]
# 	fanduel_pos = line_arr[24]
# 	team_pts = getInt(line_arr[28])
# 	opp_team_pts = getInt(line_arr[29])
	
# 	if (team_pts > opp_team_pts):
# 		won_or_lost = "W"
# 	else:
# 		won_or_lost = "L"

# 	season = get_season_from_date(game_date)
# 	team_spread = get_spread(game_id, team)
# 	implied_total = get_total(game_id, team)
# 	last_game_id = find_last_game_fantasy(table_ref, season)
# 	game_date = fixed_date(game_date)
	
# 	try:
# 		days_rest = find_days_rest_player(game_id, last_game_id, game_date, table_ref, season, team)
# 	except Exception as e:
# 		print("error!!! {0}".format(str(e)))
# 		days_rest = 100
	
# 	if days_rest is None:
# 		print("days rest is none?")
# 		days_rest = 0
	
# 	price_change_dk = get_price_change("draftkings", table_ref, draftkings_salary, last_game_id)
# 	price_change_fd = get_price_change("draftkings", table_ref, fanduel_salary, last_game_id)
# 	back_to_back = 0 if (days_rest > 0) else 1

# 	value_arr = [season, fantasy_player_id, player_id, game_id, last_name, first_name, table_ref, game_date, team, opp_team, home_or_away, starter, minutes, away_team, home_team, appeared, line_score, draftkings_pts, fanduel_pts, draftkings_salary, fanduel_salary, draftkings_value, fanduel_value, draftkings_pos, fanduel_pos, team_pts, opp_team_pts, won_or_lost, days_rest, back_to_back, team_spread, implied_total, last_game_id, price_change_dk, price_change_fd]
# 	table_arr = ["season", "fantasy_player_id", "player_id", "game_id", "last_name", "first_name", "table_ref", "game_date", "team", "opp_team", "home_or_away", "starter", "minutes", "away_team", "home_team", "appeared", "line_score", "draftkings_pts", "fanduel_pts", "draftkings_salary", "fanduel_salary", "draftkings_value", "fanduel_value", "draftkings_pos", "fanduel_pos", "team_pts", "opp_team_pts", "won_or_lost", "days_rest", "back_to_back", "team_spread", "implied_total", "last_game_id", "price_change_dk", "price_change_fd"]

# 	create_player_fantasy_table(table_ref)
# 	insert_entry("players_fantasy", table_ref, table_arr, value_arr)
# 	print("fantasy game inserted")


# def parse_fantasy_file_1516(line):
# 	line_arr = line.split(':')

# 	if (str(line_arr[0]) == "GID"):
# 		print("done!")
# 		return

# 	fantasy_player_id = line_arr[0]
# 	full_name = line_arr[1].split(", ")
# 	last_name = full_name[0]
# 	last_name = last_name.replace("'", "")
# 	last_name = last_name.replace(".", "")
# 	last_name = last_name.replace("-", "")
# 	first_name = full_name[1]
# 	first_name = first_name.replace("'", "")
# 	first_name = first_name.replace(".", "")
# 	first_name = first_name.replace("-", "")
# 	table_ref = "%s_%s" % (str(first_name), str(last_name))

# 	if (last_name == "Hilario"):
# 		table_ref = "nene_"

# 	player_id = assign_player_id(first_name, last_name)
# 	game_date = line_arr[3]
# 	team = line_arr[4]
# 	opp_team = line_arr[5]
# 	home_or_away = line_arr[6]
# 	starter = getInt(line_arr[9])
# 	minutes = getDouble(line_arr[10])
	
# 	if home_or_away == "A":
# 		away_team = team
# 		home_team = opp_team
# 	else:
# 		away_team = opp_team
# 		home_team = team

# 	game_id = assign_game_id_fantasy(away_team, home_team, game_date)
# 	appeared = getInt(line_arr[11])
# 	line_score = line_arr[17] 
# 	draftkings_pts = getDouble(line_arr[14])
# 	fanduel_pts = getDouble(line_arr[13]) 
# 	draftkings_salary = getInt(line_arr[19])
# 	fanduel_salary = getInt(line_arr[18])
# 	draftkings_value = calc_value(draftkings_salary, draftkings_pts)
# 	fanduel_value = calc_value(fanduel_salary, fanduel_pts)
# 	draftkings_pos = line_arr[23]
# 	fanduel_pos = line_arr[22]
# 	team_pts = getInt(line_arr[7])
# 	opp_team_pts = getInt(line_arr[8])

# 	if (team_pts > opp_team_pts):
# 		won_or_lost = "W"
# 	else:
# 		won_or_lost = "L"

# 	season = get_season_from_date(game_date)
# 	team_spread = get_spread(game_id, team)
# 	implied_total = get_total(game_id, team)
# 	last_game_id = find_last_game_fantasy(table_ref, season)
# 	game_date = fixed_date(game_date)
	
# 	try:
# 		days_rest = find_days_rest_player(game_id, last_game_id, game_date, table_ref, season, team)
# 	except Exception as e:
# 		print("error!!! {0}".format(str(e)))
# 		days_rest = 100
	
# 	if days_rest is None:
# 		print("days rest is none?")
# 		days_rest = 0
	
# 	price_change_dk = get_price_change("draftkings", table_ref, draftkings_salary, last_game_id)
# 	price_change_fd = get_price_change("draftkings", table_ref, fanduel_salary, last_game_id)
# 	back_to_back = 0 if (days_rest > 0) else 1

# 	value_arr = [season, fantasy_player_id, player_id, game_id, last_name, first_name, table_ref, game_date, team, opp_team, home_or_away, starter, minutes, away_team, home_team, appeared, line_score, draftkings_pts, fanduel_pts, draftkings_salary, fanduel_salary, draftkings_value, fanduel_value, draftkings_pos, fanduel_pos, team_pts, opp_team_pts, won_or_lost, days_rest, back_to_back, team_spread, implied_total, last_game_id, price_change_dk, price_change_fd]
# 	table_arr = ["season", "fantasy_player_id", "player_id", "game_id", "last_name", "first_name", "table_ref", "game_date", "team", "opp_team", "home_or_away", "starter", "minutes", "away_team", "home_team", "appeared", "line_score", "draftkings_pts", "fanduel_pts", "draftkings_salary", "fanduel_salary", "draftkings_value", "fanduel_value", "draftkings_pos", "fanduel_pos", "team_pts", "opp_team_pts", "won_or_lost", "days_rest", "back_to_back", "team_spread", "implied_total", "last_game_id", "price_change_dk", "price_change_fd"]

# 	create_player_fantasy_table(table_ref)
# 	insert_entry("players_fantasy", table_ref, table_arr, value_arr)
# 	print("fantasy game inserted")


# def assign_player_id(first_name, last_name):
# 	player_id = find_player_id(first_name, last_name)
# 	return player_id


# def assign_game_id_fantasy(away_team, home_team, game_date):
# 	away_team_id = get_team_id_from_abbrev(away_team.upper())	
# 	home_team_id = get_team_id_from_abbrev(home_team.upper())
# 	year = game_date[:4]
# 	month = game_date[4:6]
# 	day = game_date[6:8]
	
# 	if (getInt(month) > 9):
# 		season = getInt(year)
# 	else:
# 		season = getInt(year) - 1

# 	new_date = "{0}-{1}-{2}".format(year, month, day)
# 	associated_id = find_game_id(new_date, season, away_team_id, home_team_id)

# 	return associated_id

# def fixed_date(game_date):
# 	year = game_date[:4]
# 	month = game_date[4:6]
# 	day = game_date[6:8]
# 	new_date = "{0}-{1}-{2}".format(year, month, day)
# 	return new_date
	
