#All DB queries are executed from here

import psycopg2
from scripts.util import *
from datetime import datetime
from scripts.db_constants import conn, cur

#conn = psycopg2.connect("dbname=Hoops2 user=nicholascaradonna password=redacted")
#cur = conn.cursor()

def execute(query):
	try:
		cur.execute(query)
		conn.commit()
	except psycopg2.Error as e:
		print("ERROR: {0}".format(e.pgerror))

def close():
	cur.close()

def game_exists(game_id, year):
	query = "SELECT game_id FROM games_info.season_%s WHERE game_id='%s';" % (year, game_id)
	cur.execute(query)
	if (cur.fetchone() is None):
		return 0
	else:
		return 1

def player_exists(player_id):
	query = "SELECT player_id FROM players.players WHERE player_id='%s';" % player_id
	cur.execute(query)
	if (cur.fetchone() is None):
		return 0
	else:
		return 1

def official_exists(official_id):
	query = "SELECT official_id FROM officials.officials WHERE official_id='%s';" % official_id
	cur.execute(query)
	if (cur.fetchone() is None):
		return 0
	else:
		return 1

def team_exists(team_id):
	query = "SELECT team_id FROM teams.teams WHERE team_id='%s';" % team_id
	cur.execute(query)
	if (cur.fetchone() is None):
		return 0
	else:
		return 1

def get_one(query):
	cur.execute(query)
	try:
		return cur.fetchone()[0]
	except:
		return None

def get_result_set(query):
	cur.execute(query)
	try:
		desc = cur.description
		return [
        	dict(zip([col[0] for col in desc], row))
        	for row in cur.fetchall()
        ]
	except:
		return None

def insert_entry(table_name, suffix, table_params, value_params):
	num_params = len(value_params)

	if (num_params != len(table_params)):
		print("value params: {0}".format(num_params))
		print("table params: {0}".format(len(table_params)))
		print("DEADOSSSSSSSSSS")
		return 0

	queryStr = "INSERT INTO {0}.{1} (".format(table_name, suffix)
	
	for x in range(0, num_params):
		queryStr += str(table_params[x])
		if (x < (num_params - 1)):
			queryStr += ", "

	queryStr += ") VALUES ("

	for x in range(0, num_params):
		queryStr += "'"
		queryStr += str(value_params[x])
		queryStr += "'"
		if (x < (num_params - 1)):
			queryStr += ", "

	queryStr += ");"
	
	#checkQuery = "SELECT 1 from {0}.{1} WHERE game_id='{2}';".format(table_name, suffix, value_params[1])
	#cur.execute(checkQuery)

	try:
		#execute(queryStr)
		cur.execute(queryStr)
	except psycopg2.Error as e:
		try:
			print("error when adding game {0} to {1}.{2}".format(value_params[1], table_name, suffix))
		except:
			print("prob something with a coach")
		#print 'ERROR:', e[0]
		conn.rollback()
		#return
	else:
		conn.commit()
	#if not cur.fetchone():
	#	print("fer sure adding game {0} to {1}.{2}".format(value_params[1], table_name, suffix))
	#	execute(queryStr)
	#else:
	#	print(cur.fetchone())
	#	print("not adding game {0} to {1}.{2}".format(value_params[1], table_name, suffix))

def get_team_id_from_abbrev(city):
	search_query = "SELECT team_id FROM teams.teams WHERE team_abbrev='{0}'".format(city)
	return get_one(search_query)

def get_team_id_from_city(city):
	
	if (city == "LALakers"):
		search_query = "SELECT team_id FROM teams.teams WHERE team_city='{0}' AND team_nickname='{1}'".format("LosAngeles", "Lakers")
	elif (city == "LAClippers"):
		search_query = "SELECT team_id FROM teams.teams WHERE team_city='{0}' AND team_nickname='{1}'".format("LosAngeles", "Clippers")
	else:
		search_query = "SELECT team_id FROM teams.teams WHERE team_city='{0}'".format(city)
	
	return get_one(search_query)

def get_team_abbrev_from_id(team_id):
	search_query = "SELECT team_abbrev FROM teams.teams WHERE team_id='{0}'".format(team_id)
	return get_one(search_query)

def create_player_table_basic(table_ref):
	player_query = "CREATE TABLE IF NOT EXISTS players_basic.%s (id SERIAL, season integer, game_id text UNIQUE, playoff_game integer, team_id text, team_abbrev text, away_team_id text, away_team_abbrev text, home_team_id text, home_team_abbrev text, my_team_score integer, opp_team_score integer, home_or_away text, won_or_lost text, player_id text, player_name text, start_position character, comment text, min decimal, fgm integer, fga integer, fg_pct decimal, threepm integer, threepa integer, threep_pct decimal, ftm integer, fta integer,ft_pct decimal, o_reb integer, d_reb integer, reb integer, assist integer, tov integer, stl integer, blk integer, pf integer, pts integer, plus_minus integer, PRIMARY KEY(game_id));" % table_ref
	execute(player_query)

def insert_player(player_id, first_name, last_name, table_ref):
	check_query = "SELECT 1 FROM players.players WHERE player_id='{0}'".format(player_id)
	cur.execute(check_query)
	if not cur.fetchone():
		insert_query = "INSERT INTO players.players (player_id, first_name, last_name, table_ref) VALUES ('%s', '%s', '%s', '%s')" % (player_id, first_name, last_name, table_ref)
		execute(insert_query)

def insert_official(official_id, first_name, last_name, table_ref):
	check_query = "SELECT 1 FROM officials.officials WHERE official_id='{0}'".format(official_id)
	cur.execute(check_query)
	if not cur.fetchone():
		insert_query = "INSERT INTO officials.officials (official_id, first_name, last_name, table_ref) VALUES ('%s', '%s', '%s', '%s');" % (official_id, first_name, last_name, table_ref)
		execute(insert_query)

def insert_team(team_id, team_abbrev, team_city, team_nickname):
	check_query = "SELECT 1 FROM teams.teams WHERE team_id='{0}'".format(team_id)
	cur.execute(check_query)
	if not cur.fetchone():
		insert_query = "INSERT INTO teams.teams (team_id, team_abbrev, team_city, team_nickname) VALUES ('%s', '%s', '%s', '%s');" % (team_id, team_abbrev, team_city, team_nickname)
		execute(insert_query)

def create_team_table_info(team_abbrev):
	team_query = "CREATE TABLE IF NOT EXISTS teams_info.{0} (id SERIAL, season integer, game_id text UNIQUE, game_date text, playoff_game integer, game_num integer, team_id text, coach_name text, home_or_away text, opp_team_id text, opp_team_abbrev text, official_1_id text, official_2_id text, official_3_id text, official_4_id text, last_meeting_id text, won_or_lost text, team_score integer, opp_team_score integer, overtime integer, attendance integer, team_pts_1q integer, team_pts_2q integer, team_pts_3q integer, team_pts_4q integer, team_pts_ot1 integer, team_pts_ot2 integer, team_pts_ot3 integer, team_pts_ot4 integer, opp_team_pts_1q integer, opp_team_pts_2q integer, opp_team_pts_3q integer, opp_team_pts_4q integer, opp_team_pts_ot1 integer, opp_team_pts_ot2 integer, opp_team_pts_ot3 integer, opp_team_pts_ot4 integer, PRIMARY KEY (game_id));".format(str(team_abbrev))
	execute(team_query)

def create_team_table_basic(team_abbrev):
	team_query = "CREATE TABLE IF NOT EXISTS teams_basic.{0} (id SERIAL, season integer, game_id text UNIQUE, team_id text, team_abbrev text, away_team_id text, away_team_abbrev text, home_team_id text, home_team_abbrev text, team_score integer, opp_team_score integer, home_or_away text, won_or_lost text, fgm integer, fga integer, fg_pct decimal, threepm integer, threepa integer, threep_pct decimal, ftm integer, fta integer, ft_pct decimal, o_reb integer, d_reb integer, reb integer, assist integer, tov integer, stl integer, blk integer, pf integer, pts integer, plus_minus integer, FOREIGN KEY(game_id) REFERENCES teams_info.{1} );".format(team_abbrev, team_abbrev)
	execute(team_query)

def create_team_table_advanced(team_abbrev):
	team_adv_query = "CREATE TABLE IF NOT EXISTS teams_advanced.{0} (id SERIAL, season integer, game_id text UNIQUE, team_id text, team_abbrev text, minutes decimal, o_rtg decimal, d_rtg decimal, net_rtg decimal, ast_pct decimal, ast_to_ratio decimal, ast_ratio decimal, o_reb_pct decimal, d_reb_pct decimal, reb_pct decimal, tov_pct decimal, efg_pct decimal, ts_pct decimal, usage_pct decimal, pace decimal, pie decimal, FOREIGN KEY(game_id) REFERENCES teams_info.{1} );".format(team_abbrev, team_abbrev) 
	execute(team_adv_query)

def create_player_table_advanced(table_ref):
	player_adv_query = "CREATE TABLE IF NOT EXISTS players_advanced.{0} (id SERIAL, season integer, game_id text UNIQUE, player_id text, player_name text, minutes decimal, o_rtg decimal, d_rtg decimal, net_rtg decimal, ast_pct decimal, ast_to_ratio decimal, ast_ratio decimal, o_reb_pct decimal, d_reb_pct decimal, reb_pct decimal, tov_pct decimal, efg_pct decimal, ts_pct decimal, usage_pct decimal, pace decimal, pie decimal, FOREIGN KEY(game_id) REFERENCES players_basic.{1} );".format(table_ref, table_ref) 
	execute(player_adv_query)

def create_player_fantasy_table(table_ref):
	fantasy_query = "CREATE TABLE IF NOT EXISTS players_fantasy.{0} (id SERIAL, season integer, fantasy_player_id integer, player_id text, game_id text UNIQUE, last_name text, first_name text, table_ref text, game_date text, team text, opp_team text, home_or_away text, starter integer, minutes decimal, away_team text, home_team text, appeared integer, line_score text, draftkings_pts decimal, fanduel_pts decimal, draftkings_salary decimal, fanduel_salary decimal, draftkings_value decimal, fanduel_value decimal, draftkings_pos text, fanduel_pos text, team_pts integer, opp_team_pts integer, won_or_lost text, days_rest integer, back_to_back integer, team_spread decimal, implied_total decimal, last_game_id text, price_change_dk integer, price_change_fd integer, FOREIGN KEY(game_id) REFERENCES players_basic.{1});".format(table_ref, table_ref)
	execute(fantasy_query)

def create_teams_betting_table(table_ref):
	betting_query = "CREATE TABLE IF NOT EXISTS teams_betting.{0} (id SERIAL, season integer, game_id text UNIQUE, team_id text, opp_team_id text, playoff_game integer, wl_outright text, team_abbrev text, opp_team_abbrev text, away_team_city text, home_team_city text, home_or_away text, fav integer, dog integer, pickem integer, line_open decimal, line_close decimal, line_movement decimal, total_open decimal, total_close decimal, total_movement decimal, team_ml integer, opp_ml integer, team_actual_total integer, opp_team_actual_total integer, actual_total integer, team_days_rest integer, opp_team_days_rest integer, team_btb integer, opp_team_btb integer, over_under_result text, ats_result text, last_meeting_id text)".format(table_ref)
	execute(betting_query)

def create_player_table_tracking(table_ref):
	track_query = "CREATE TABLE IF NOT EXISTS players_tracking.{0} (id SERIAL, season integer, game_id text UNIQUE, player_id text, player_name text, minutes decimal, dist_trav decimal, avg_speed decimal, touches integer, passes integer, assist integer, sec_ast integer, ft_ast integer, d_atrim_fgm integer, d_atrim_fga integer, d_atrim_fg_pct decimal, oreb_chance integer, dreb_chance integer, reb_chance integer, fg_pct decimal, cfgm integer, cfga integer, cfg_pct decimal, ufgm integer, ufga integer, ufga_pct decimal, FOREIGN KEY(game_id) REFERENCES players_basic.{1} );".format(str(table_ref), str(table_ref))
	execute(track_query)

def create_team_table_tracking(team_abbrev):
	track_query = "CREATE TABLE IF NOT EXISTS teams_tracking.{0} (id SERIAL, season integer, game_id text UNIQUE, team_id text, team_abbrev text, dist_trav decimal, touches integer, passes integer, assist integer, sec_ast integer, ft_ast integer, d_atrim_fgm integer, d_atrim_fga integer, d_atrim_fg_pct decimal, oreb_chance integer, dreb_chance integer, reb_chance integer, cfgm integer, cfga integer, cfg_pct decimal, ufgm integer, ufga integer, ufga_pct decimal, FOREIGN KEY(game_id) REFERENCES teams_info.{1} );".format(str(team_abbrev), str(team_abbrev))
	execute(track_query)

def create_player_table_scoring(table_ref):
	score_query = "CREATE TABLE IF NOT EXISTS players_scoring.{0} (id SERIAL, season integer, game_id text UNIQUE, player_id text, player_name text, minutes decimal, pct_pts_2pt decimal, pct_pts_midrange decimal, pct_pts_fastbreak decimal, pts_pts_ft decimal, pct_pts_offto decimal, pct_pts_paint decimal, pct_2ptfgm_ast decimal, pct_2ptfgm_uast decimal, pct_3ptfgm_ast decimal, pct_3ptfgm_uast decimal, pct_fgm_ast decimal, pct_fgm_uast decimal, FOREIGN KEY(game_id) REFERENCES players_basic.{1})".format(table_ref, table_ref)
	execute(score_query)

def create_team_table_scoring(team_abbrev):
	score_query = "CREATE TABLE IF NOT EXISTS teams_scoring.{0} (id SERIAL, season integer, game_id text UNIQUE, team_id text, team_abbrev text, pct_fga_2pt decimal, pct_fga_3pt decimal, pct_pts_2pt decimal, pct_pts_midrange decimal, pct_pts_fastbreak decimal, pts_pts_ft decimal, pct_pts_offto decimal, pct_pts_paint decimal, pct_2ptfgm_ast decimal, pct_2ptfgm_uast decimal, pct_3ptfgm_ast decimal, pct_3ptfgm_uast decimal, pct_fgm_ast decimal, pct_fgm_uast decimal, FOREIGN KEY(game_id) REFERENCES teams_info.{1})".format(team_abbrev, team_abbrev)
	execute(score_query)

def create_player_table_usage(table_ref):
	usg_query = "CREATE TABLE IF NOT EXISTS players_usage.{0} (id SERIAL, season integer, game_id text UNIQUE, team_id text, player_id text, player_name text, minutes decimal, usg_pct decimal, pct_fgm decimal, pct_fga decimal, pct_3pm decimal, pct_3pa decimal, pct_ftm decimal, pct_fta decimal, pct_oreb decimal, pct_dreb decimal, pct_reb decimal, pct_ast decimal, pct_tov decimal, pct_stl decimal, pct_blk decimal, pct_blka decimal, pct_pf decimal, pct_pfd decimal, pct_pts decimal, FOREIGN KEY(game_id) REFERENCES players_basic.{1})".format(table_ref, table_ref)
	execute(usg_query)

def find_game_id(game_date, season, away_team_id, home_team_id):
	search_query = "SELECT game_id FROM games_info.season_{0} WHERE game_date='{1}' AND away_team_id='{2}' AND home_team_id='{3}'".format(season, game_date, away_team_id, home_team_id)
	return get_one(search_query)

def find_game_date(game_id, team_abbrev):
	search_query = "SELECT game_date FROM teams_info.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	return get_one(search_query)

def find_player_id(first_name, last_name):
	search_query = "SELECT player_id FROM players.players WHERE first_name='{0}' AND last_name='{1}'".format(first_name, last_name)
	return get_one(search_query)

def get_last_meeting_id(game_id, season):
	search_query = "SELECT last_meeting_id FROM games_info.season_{0} WHERE game_id='{1}'".format(season, game_id) 
	return get_one(search_query)

def find_game_num(team_abbrev, season):
	try:
		search_query = "SELECT count(*) FROM teams_info.{0} WHERE season='{1}'".format(team_abbrev, season)
		result = get_one(search_query)
		if result is None:
			return 0
		else:
			return result
	except:
		conn.rollback()
		return 0

def find_game_num_from_id(team_abbrev, game_id):
	search_query = "SELECT game_num FROM teams_info.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	result = get_one(search_query)
	if result is None:
		return 0
	else:
		return result

def find_days_rest_team(team_abbrev, game_id):
	date_query = "SELECT game_date FROM teams_info.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	num_query = "SELECT game_num FROM teams_info.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	later_date = get_one(date_query)
	later_num = getInt(get_one(num_query))

	if later_num == 1:
		return 100

	earlier_num = later_num - 1
	earlier_date_query = "SELECT game_date FROM teams_info.{0} WHERE game_num='{1}'".format(team_abbrev, earlier_num)
	earlier_date = get_one(earlier_date_query)

	return days_between(earlier_date, later_date) - 1

def find_days_rest_player(game_id, last_game_id, game_date, table_ref, season, team_abbrev):	

	num_query = "SELECT game_num FROM teams_info.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	num = getInt(get_one(num_query))
	if num == 1:
		return 100

	try:
		minutes = getDouble(get_one("SELECT minutes FROM players_fantasy.{0} WHERE game_id='{1}'".format(table_ref, last_game_id)))
	except:
		conn.rollback()
		print("conn rollback on game_id".format(game_id))		
		print("conn rollback on last game id".format(last_game_id))
		return 100

	last_date = get_one("SELECT game_date FROM players_fantasy.{0} WHERE game_id='{1}'".format(table_ref, last_game_id))
	days_btwn = days_between(last_date, game_date)

	if (minutes == 0):
		last_days_rest = getInt(get_one("SELECT days_rest FROM players_fantasy.{0} WHERE game_id='{1}'".format(table_ref, last_game_id)))
		result = last_days_rest + days_btwn
	else:
		result = days_btwn - 1

	return result


def get_spread(game_id, team_abbrev):
	dog_query = "SELECT dog FROM teams_betting.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	dog = get_one(dog_query)
	fav_query = "SELECT fav FROM teams_betting.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	fav = get_one(fav_query)
	pickem_query = "SELECT pickem FROM teams_betting.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	pickem = get_one(pickem_query)
	spread_query = "SELECT line_close FROM teams_betting.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	spread = getDouble(get_one(spread_query))

	if getInt(dog) == 1:
		return spread
	elif getInt(fav) == 1:
		return spread * -1
	elif getInt(pickem) == 1:
		return 0
	else:
		return None


def get_total(game_id, team_abbrev):
	total_query = "SELECT total_close FROM teams_betting.{0} WHERE game_id='{1}'".format(team_abbrev, game_id)
	return getDouble(get_one(total_query))

def find_last_game_fantasy(table_ref, season):
	try:
		last_id_query = "SELECT max(id) FROM players_fantasy.{0} WHERE season='{1}'".format(table_ref, season)
		last_id = get_one(last_id_query)
		game_id_query = "SELECT game_id FROM players_fantasy.{0} WHERE id='{1}'".format(table_ref, last_id)
		return get_one(game_id_query)
	except:
		conn.rollback()
		return None

def get_price_change(site, table_ref, current_price, last_game_id):
	try:
		last_price_query = "SELECT {0}_salary FROM players_fantasy.{1} WHERE game_id='{2}'".format(site, table_ref, last_game_id)
		last_price = getInt(get_one(last_price_query))
		price_diff = current_price - last_price
		return price_diff
	except:
		conn.rollback()
		return 0

def check_if_playoffs(season, game_id):
	check_query = "SELECT playoff_game FROM games_info.season_{0} WHERE game_id='{1}'".format(season, game_id)
	return get_one(check_query)

def get_ast_to_ratio(table_ref, game_id, prefix):
	ast_query = "SELECT assist FROM {0}.{1} WHERE game_id='{2}'".format(prefix, table_ref, game_id)
	ast = getDouble(get_one(ast_query))
	tov_query = "SELECT tov FROM {0}.{1} WHERE game_id='{2}'".format(prefix, table_ref, game_id)
	tov = getDouble(get_one(tov_query))

	if tov == 0:
		result = ast
	elif ast == 0:
		result = tov
	else:
		result = getDouble(ast / tov)

	return result

def find_team_win_pct(team_abbrev, game_id, incl_playoffs):
	season_query = "SELECT season FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id)
	season = get_one(season_query)
	game_num_query = "SELECT game_num FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id)
	game_num = get_one(game_num_query)

	if (incl_playoffs):
		cur.execute("SELECT won_or_lost FROM teams_info.{0} WHERE season='{1}' AND game_num < {2}".format(team_abbrev, season, game_num))
	else:
		cur.execute("SELECT won_or_lost FROM teams_info.{0} WHERE season='{1}' AND playoff_game=0 AND game_num < {2}".format(team_abbrev, season, game_num))

	wins = 0
	losses = 0

	for result in cur.fetchall():
		if (result[0] == "W"):
			wins += 1
		elif (result[0] == "L"):
			losses += 1

	try:
		return getDouble(getDouble(wins) / getDouble(wins + losses))
	except:
		return 0

#for django
def get_coach_list():
	search_query = "SELECT coach_name FROM coaches.coaches"
	results = get_result_set(search_query)
	new_results = []
	new_results.append("All")
	for x in range(0, len(results)):
		new_results.append(results[x]['coach_name'])

	return zip(new_results, new_results)

def get_official_list():	
	search_query = "SELECT * FROM officials.officials"
	results = get_result_set(search_query)
	official_names = []
	official_ids = []
	official_names.append("All")
	official_ids.append("All")
	for x in range(0, len(results)):
		full_name = "{0} {1}".format(results[x]['first_name'], results[x]['last_name'])
		official_names.append(full_name)
		official_ids.append(results[x]['official_id'])

	return zip(official_ids, official_names)

	