#Random utility functions needed throughout script

from decimal import Decimal
from scipy import stats
import math
import numpy as np
import pandas as pd
from datetime import datetime

east_teams = ['ATL', 'BKN', 'BOS', 'CHA', 'CHI', 'CLE', 'DET', 'IND', 'MIA', 'MIL', 'NYK', 'ORL', 'PHI', 'TOR', 'WAS']
west_teams = ['DAL', 'DEN', 'GSW', 'HOU', 'LAC', 'LAL', 'MEM', 'MIN', 'NOP', 'OKC', 'POR', 'PHX', 'SAC', 'SAS', 'UTA']


def convertMinutes(mins):
	if (str(mins) == "None"):
		return 0
	else:
		stringArr = mins.split(':')
		minutes = int(stringArr[0])
		seconds = int(stringArr[1])
		ratioSec = Decimal(Decimal(seconds) / 60)
		return Decimal(minutes + ratioSec)

def getInt(check):
	try:
		if (math.isnan(float(check))):
			return 0
		else:
			return int(check)
	except:
		return 0


def getDouble(check):
	try:
		if (math.isnan(float(check))):
			return 0
		else:
			return Decimal(check)
	except:
		return 0


def game_happened(game_id):
	try:
		box = game.BoxscoreSummary(game_id)
		return 1
	except:
		return 0

def days_between(d1, d2):
    d1 = datetime.strptime(str(d1), "%Y-%m-%d")
    d2 = datetime.strptime(str(d2), "%Y-%m-%d")
    return abs((d2 - d1).days)
	
#def calc_draftkings_pts(stats_arr):
	#todo

#def calc_fanduel_pts(stats_arr):
	#todo

def calc_value(salary, score):
	try:
		divisor = getDouble(salary) / 1000
		value_score = getDouble(score / divisor)
		return value_score
	except:
		return 0

def get_season_from_date(game_date):
	year = getInt(game_date[:4])
	
	if (game_date[4] == "0"):
		year = year - 1

	return year

def get_table_ref(player_name):
	name_arr = str(player_name).split(" ")
	
	first_name = name_arr[0]
	if len(name_arr) < 2:
		last_name = ""
	else:
		last_name = name_arr[1]

	table_ref = "%s_%s" % (first_name, last_name)	

	return(table_ref)

def calc_pct(divisor, dividend):
	if (divisor == 0):
		return 0

	if (dividend == 0):
		return divisor

	return getDouble(divisor / dividend)

def is_east(team_abbrev):
	return (team_abbrev in east_teams)

def is_west(team_abbrev):
	return (team_abbrev in west_teams)

def get_east_abbrev_list():
	return east_teams

def get_west_abbrev_list():	
	return west_teams

def get_all_teams_list():
	all_teams_list = east_teams + west_teams
	return all_teams_list

def manipulate_result_set(result_set):
	print("begin manip result set")
	for result in result_set:
		result['team_actual_total'] = float(result['team_actual_total'])
		result['opp_team_actual_total'] = float(result['opp_team_actual_total'])
		result['team_days_rest'] = float(result['team_days_rest'])
		result['opp_team_days_rest'] = float(result['opp_team_days_rest'])
		result['total_close'] = float(result['total_close'])
		result['total_open'] = float(result['total_open'])
		result['total_movement'] = float(result['total_movement'])
		result['actual_total'] = float(result['actual_total'])
		result['line_open'] = float(result['line_open'])
		result['line_close'] = float(result['line_close'])
		result['line_movement'] = float(result['line_movement'])
		result['total_difference'] = float(float(result['actual_total']) - float(result['total_close']))
		result['margin_of_victory'] = float(result['team_actual_total'] - result['opp_team_actual_total'])

		if (result['fav'] == 1):
			if (result['wl_outright'] == "W"):
				result['spread_difference'] = float(result['margin_of_victory']) - result['line_close']
			else:
				result['spread_difference'] = float(result['margin_of_victory']) - result['line_close']
		elif (result['dog'] == 1):
			if (result['wl_outright'] == "W"):
				result['spread_difference'] = float(result['margin_of_victory']) + result['line_close']
			else:
				result['spread_difference'] = float(result['margin_of_victory']) + result['line_close']
		else:
			if (result['wl_outright'] == "W"):
				result['spread_difference'] = result['margin_of_victory']
			else:
				result['spread_difference'] = float(result['margin_of_victory'] * -1)

	print("finish manip result set")
	return result_set


def rsquared(x, y):
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    return r_value**2

def find_mean(vals):
	return float(sum(vals)/len(vals))
