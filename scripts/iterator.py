#iterate through game ids
from __future__ import print_function
from nba_py import game
import psycopg2
import pandas
from scripts.util import *
from scripts.db_functions import *
from scripts.one_game_insert import *
from scripts.fantasy import *
from scripts.betting import *
import csv

def add_game_to_db(id, season, playoffs):
	if (game_happened(id) == 0):
		print("game %s does not exist" % id)
		return

	parse_info(id, season, playoffs)
	parse_basic(id, season)
	parse_advanced(id, season)
	parse_tracking(id, season)
	parse_scoring(id, season)
	parse_usage(id, season)
	

def insert_season(season, start):
	game_id = start

	for x in range (0, 1230):
		add_game_to_db(game_id, season, 0)
		game_id = '00' + str(int(game_id) + 1)
	
	print("donezo")


def insert_playoffs(season, base_id):
	for playoff_round in range (1, 5):
		for playoff_series in range(0, 8):
			for series_game in range(1, 8):
						game_id = "%s%s%s%s" % (base_id, playoff_round, playoff_series, series_game)
						add_game_to_db(game_id, season, 1)


def insert_fantasy(path, season):
	for line in reversed(list(open(path))):
		print("PARSE {0}".format(line))
		if (season == 2014):
			parse_fantasy_file_1415(line)
		else:
			parse_fantasy_file_1516(line)


	print("we out")

def insert_betting(path, season):
	c = csv.reader(open(path, 'rU'), dialect=csv.excel_tab)
	next(c, None)
	for row in c: 
		away_row = row
		home_row = c.next()
		parse_betting(str(away_row[0]), str(home_row[0]), season)
