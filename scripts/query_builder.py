#Build pieces of queries to be used for games based on user specified parameters

from scripts.db_functions import *
from scripts.util import *
from datetime import *
from scripts.db_constants import conn, cur

#conn = psycopg2.connect("dbname=Hoops2 user=nicholascaradonna password=redacted")
#cur = conn.cursor()

def additional_param_builder(params):
	query_str = ""

	if (params['playoffs_only'] == 1):
		query_str += " AND playoff_game = 1"

	if (params['reg_season_only'] == 1):
		query_str += " AND playoff_game = 0"

	if (params['days_rest'] == 1):
		query_str += " AND team_days_rest >= {0} AND team_days_rest <= {1}".format(params['days_rest_min'], params['days_rest_max'])

	if (params['opp_days_rest'] == 1):
		query_str += " AND opp_team_days_rest >= {0} AND opp_team_days_rest <= {1}".format(params['opp_days_rest_min'], params['opp_days_rest_max'])

	if (params['fav_or_dog'] == 1):
		if (params['fav'] == 1):
			query_str += " AND fav = 1"
		elif (params['pickem'] == 1):
			query_str += " AND pickem = 1"
		elif (params['dog'] == 1):
			query_str += " AND dog = 1"

	if (params['home_or_away'] == 1):
		if (params['home'] == 1):
			query_str += " AND home_or_away = 'home'"
		if (params['away'] == 1):
			query_str += " AND home_or_away = 'away'"

	if (params['vs_conference'] == 1):
		if (params['vs_east'] == 1):
			
			east_str = "{"
			
			for abbrev in get_east_abbrev_list():
				east_str += abbrev
				east_str += ","

			east_str = east_str[:-1]
			east_str += "}"

			query_str += " AND opp_team_abbrev = ANY ('{0}'::text[])".format(east_str)
		if (params['vs_west'] == 1):
			
			west_str = "{"
			
			for abbrev in get_west_abbrev_list():
				west_str += abbrev
				west_str += ","

			west_str = west_str[:-1]
			west_str += "}"

			query_str += " AND opp_team_abbrev = ANY ('{0}'::text[])".format(west_str)

	if (params['opp_team'] is not None):
		print(params['opp_team'])
		query_str += " AND opp_team_abbrev = '{0}'".format(params['opp_team'])

	if (params['line_close'] == 1):
		query_str += " AND line_close >= {0} AND line_close <= {1}".format(params['line_close_min'], params['line_close_max'])

	if (params['line_open'] == 1):
		query_str += " AND line_open >= {0} AND line_open <= {1}".format(params['line_open_min'], params['line_open_max'])

	if (params['line_movement'] == 1):
		query_str += " AND line_movement >= {0} AND line_movement <= {1}".format(params['line_movement_min'], params['line_movement_max'])

	if (params['total_open'] == 1):
		query_str += " AND total_open_min >= {0} AND total_open_max <= {1}".format(params['total_open_min'], params['total_open_max'])

	if (params['total_close'] == 1):
		query_str += " AND total_close_min >= {0} AND total_close_max <= {1}".format(params['total_close_min'], params['total_close_max'])

	if (params['total_movement'] == 1):
		query_str += " AND total_movement >= {0} AND total_movement <= {1}".format(params['total_movement_min'], params['total_movement_max'])

	if (params['wl_outright'] == 1):
		query_str += " AND wl_outright = '{0}'".format(params['wl_outright_result'])

	if (params['ats'] == 1):
		query_str += " AND ats_result = '{0}'".format(params['ats_result'])

	if (params['over_under'] == 1):
		query_str += " AND over_under_result = '{0}'".format(params['over_under_result'])

	return query_str


def last_game_param_parser(last_game_params, game_id_list, team_abbrev, last_game_tracking):
	temp_list = []

	print("start last game parser")
	for game_id in game_id_list:
		curr_game_id = get_one("SELECT id FROM teams_betting.{0} WHERE game_id={1}".format(team_abbrev, game_id))
		last_game_id = get_one("SELECT game_id FROM teams_betting.{0} WHERE id={1}".format(team_abbrev, curr_game_id - 1))
		
		if last_game_id is None:
			continue

		last_game_info = get_result_set("SELECT * FROM teams_betting.{0} WHERE game_id='{1}'".format(team_abbrev, last_game_id))[0]
		opp_team_abbrev = last_game_info['opp_team_abbrev']
		margin_of_victory = last_game_info['team_actual_total'] - last_game_info['opp_team_actual_total']
		total_difference = float(last_game_info['actual_total']) - float(last_game_info['total_close'])
		if (last_game_info['fav'] == 1):
			if (last_game_info['wl_outright'] == "W"):
				spread_difference = float(margin_of_victory) - float(last_game_info['line_close'])
			else:
				spread_difference = float(margin_of_victory) - float(last_game_info['line_close'])
		elif (last_game_info['dog'] == 1):
			if (last_game_info['wl_outright'] == "W"):
				spread_difference = float(margin_of_victory) + float(last_game_info['line_close'])
			else:
				spread_difference = float(margin_of_victory) + float(last_game_info['line_close'])
		else:
			if (last_game_info['wl_outright'] == "W"):
				spread_difference = margin_of_victory
			else:
				spread_difference = float(margin_of_victory * -1)

		if (last_game_params['home_or_away'] == 1):
			home_or_away = last_game_info['home_or_away']
			if (last_game_params[home_or_away] == 0):
				continue

		if (last_game_params['vs_conference'] == 1):
			if (last_game_params['vs_east'] == 1):
				if (is_west(opp_team_abbrev)):
					continue
			elif (last_game_params['vs_west'] == 1):
				if (is_east(opp_team_abbrev)):
					continue

		if (last_game_params['opp_team'] is not None):
			if last_game_params['opp_team'] != opp_team_abbrev:
				continue

		if (last_game_params['fav_or_dog'] == 1):
			if (last_game_params['fav'] == 1):
				if (last_game_info['fav'] == 0):
					continue
			elif (last_game_params['dog'] == 1):
				if (last_game_info['dog'] == 0):
					continue	
			elif (last_game_params['pickem'] == 1):
				if (last_game_info['pickem'] == 0):
					continue

		if (last_game_params['wl_outright'] == 1):
			if (last_game_params['wl_outright_result'] != last_game_info['wl_outright']):
				continue

		if (last_game_params['ats'] == 1):
			if (last_game_params['ats_result'] != last_game_info['ats_result']):
				continue

		if (last_game_params['over_under'] == 1):
			if (last_game_params['over_under_result'] != last_game_info['over_under_result']):
				continue

		if (last_game_params['team_actual_total'] == 1):
			if (last_game_info['team_actual_total'] < int(last_game_params['team_actual_total_min'])):
				continue
			elif (last_game_info['team_actual_total'] > int(last_game_params['team_actual_total_max'])):
				continue

		if (last_game_params['opp_team_actual_total'] == 1):
			if (last_game_info['opp_team_actual_total'] < getInt(last_game_params['opp_team_actual_total_min'])):
				continue
			elif (last_game_info['opp_team_actual_total'] > getInt(last_game_params['opp_team_actual_total_max'])):
				continue

		if (last_game_params['opp_record'] == 1):
			opp_team_win_pct = find_team_win_pct(opp_team_abbrev, last_game_id, False)
			if (opp_team_win_pct < float(last_game_params['opp_record_min_win_pct'])):
				continue
			if (opp_team_win_pct > float(last_game_params['opp_record_max_win_pct'])):
				continue

		if (last_game_params['margin_of_victory'] == 1):
			if (margin_of_victory < getInt(last_game_params['margin_of_victory_min'])):
				continue
			if (margin_of_victory > getInt(last_game_params['margin_of_victory_max'])):
				continue

		if (last_game_params['spread_difference'] == 1):
			#print("spread diff min {0}".format(last_game_params['spread_difference_min']))
			#print("spread diff max {0}".format(last_game_params['spread_difference_max']))
			#print("spread diff actual {0}".format(spread_difference))
			if (float(spread_difference) < float(last_game_params['spread_difference_min'])):
				continue
			if (float(spread_difference) > float(last_game_params['spread_difference_max'])):
				continue

		if (last_game_params['total_difference'] == 1):
			if (float(total_difference) < float(last_game_params['total_difference_min'])):
				continue
			if (float(total_difference) > float(last_game_params['total_difference_max'])):
				continue

		# if (last_game_tracking['on'] == 1):
		# 	if (check_tracking_params(last_game_id, team_abbrev, last_game_tracking) is False):
		# 		continue

		temp_list.append(game_id)

	print("end last game parser")
	return temp_list

def check_tracking_params(game_id, team_abbrev, last_game_tracking):
	tracking_info = get_result_set("SELECT * FROM teams_tracking.{0} WHERE game_id='{1}'".format(team_abbrev, last_game_id))[0]

	bool_val = True

	if (last_game_tracking['dist_trav'] == 1):
		if (tracking_info['dist_trav'] < last_game_tracking['dist_trav_min']):
			bool_val = False
		if (tracking_info['dist_trav'] > last_game_tracking['dist_trav_max']):
			bool_val = False

	if (last_game_tracking['cfg_pct'] == 1):
		if (tracking_info['cfg_pct'] < last_game_tracking['cfg_pct_min']):
			bool_val = False
		if (tracking_info['cfg_pct'] > last_game_tracking['cfg_pct_max']):
			bool_val = False

	return bool_val

def get_game_id_list(team_abbrev, game_num, seasons):
	if (len(seasons) == 0):
		id_query = "SELECT game_id FROM teams_info.{0}".format(team_abbrev)
	elif (len(seasons) == 1):
		id_query = "SELECT game_id FROM teams_info.{0} WHERE game_num < {1} AND season={2}".format(team_abbrev, game_num, seasons[0])
	else:
		id_query = "SELECT game_id FROM teams_info.{0} WHERE (game_num < {1} AND season={2}) OR (season IN '{3}')".format(team_abbrev, game_num, seasons[-1], "(" + ','.join(map(str, seasons[:-1])) + ")")

	#cur.execute(id_query)
	#temp_list = []

	# try:
	cur.execute(id_query)
	temp_list = cur.fetchall()
	#temp_list = get_result_set(id_query)
	# except:
	# 	conn.rollback()
	# 	cur.close()
	# 	conn.close()

	#conn.commit()

	id_list = []


	for item in temp_list:
		id_list.append(str("'" + item[0] + "'"))

	return id_list

def apply_record_filter(team_abbrev, game_id_list, min_pct, max_pct, self_or_opp):	
	new_id_list = []

	if (self_or_opp == "opp"):
		for game_id in game_id_list:
			
			opp_team_abbrev = get_one("SELECT opp_team_abbrev FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id))
			opp_team_win_pct = find_team_win_pct(opp_team_abbrev, game_id, False)	

			if (float(opp_team_win_pct) < float(min_pct)):
				continue
			elif (float(opp_team_win_pct) > float(max_pct)):
				continue
			else:
				new_id_list.append(str(game_id))
	else:
		for game_id in game_id_list:
			team_win_pct = find_team_win_pct(team_abbrev, game_id, False)		
			if (float(team_win_pct) < float(min_pct)):
				continue
			elif (float(team_win_pct) > float(max_pct)):
				continue
			else:
				new_id_list.append(str(game_id))


	return new_id_list

def apply_coach_filter(team_abbrev, game_id_list, coach_name):
	new_id_list = []
	for game_id in game_id_list:
		actual_coach = get_one("SELECT coach_name FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id))
		if (actual_coach == coach_name):
			new_id_list.append(str(game_id))
		else:
			continue

	return new_id_list

def apply_official_filter(team_abbrev, game_id_list, official_id):
	new_id_list = []	
	for game_id in game_id_list:
		official_1 = get_one("SELECT official_1_id FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id))
		official_2 = get_one("SELECT official_2_id FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id))
		official_3 = get_one("SELECT official_3_id FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id))
		official_4 = get_one("SELECT official_4_id FROM teams_info.{0} WHERE game_id={1}".format(team_abbrev, game_id))
		officials = [official_1, official_2, official_3, official_4]
		if (official_id in officials):
			new_id_list.append(str(game_id))
				
	return new_id_list

def get_allteams_games(start_date, end_date, addtl_params, last_params, last_game_tracking):
	total_result_set = []
	eligible_team_list = []
	print("begin get allteams games")
	
	if (addtl_params['is_conference'] == 1):
		if (addtl_params['east'] == 1):
			eligible_team_list = get_east_abbrev_list()
		elif (addtl_params['west'] == 1):
			eligible_team_list = get_west_abbrev_list()
	else:
		eligible_team_list = get_all_teams_list()

	if ((start_date is not None) and (end_date is not None)):
		start_date_arr = start_date.split("-")
		start_date_dt = date(start_date_arr[0], start_date_arr[1], start_date_arr[2])
		end_date_arr = end_date.split("-")
		end_date_dt = date(end_date_arr[0], end_date_arr[1], end_date_arr[2])
		#todo
	else:
		#just for season(s)
		for team_abbrev in eligible_team_list:
			
			elig_game_id_list = get_game_id_list(team_abbrev, 83, addtl_params['season'])
			if addtl_params['on'] == 1:
				if (addtl_params['opp_record'] == 1):
					elig_game_id_list = apply_record_filter(team_abbrev, elig_game_id_list, addtl_params['opp_record_min_win_pct'], addtl_params['opp_record_max_win_pct'], "opp")
				if (addtl_params['team_record'] == 1):
					elig_game_id_list = apply_record_filter(team_abbrev, elig_game_id_list, addtl_params['team_record_min_win_pct'], addtl_params['team_record_max_win_pct'], "self")
				if (addtl_params['coach_filter'] != "All"):
					elig_game_id_list = apply_coach_filter(team_abbrev, elig_game_id_list, addtl_params['coach_filter'])
				if (addtl_params['official_filter'] != "All"):
					elig_game_id_list = apply_official_filter(team_abbrev, elig_game_id_list, addtl_params['official_filter'])

			if (last_params['on'] == 1):
				elig_game_id_list = last_game_param_parser(last_params, elig_game_id_list, team_abbrev, last_game_tracking)

			if addtl_params['on'] == 1:
				addtl_param_query = additional_param_builder(addtl_params)
			else:
				addtl_param_query = ""

			if elig_game_id_list is None:
				print("nunya")
				return None

			total_query = "SELECT * FROM teams_betting.{0} WHERE game_id IN {1}{2}".format(team_abbrev, "(" + ','.join(map(str, elig_game_id_list)) + ")", addtl_param_query)
			team_result_set = get_result_set(total_query)

			total_result_set = total_result_set + team_result_set

	print("end get allteams games")
	return total_result_set

def add_advanced_stats(result_set):
	print("begin add advanced")
	for result in result_set:
		result['off_rating'] = float(get_advanced(result['team_abbrev'], result['game_id'], "o_rtg"))
		result['def_rating'] = float(get_advanced(result['team_abbrev'], result['game_id'], "d_rtg"))
		result['net_rating'] = float(get_advanced(result['team_abbrev'], result['game_id'], "net_rtg"))
	print("end add advanced")
	return result_set

def get_advanced(team_abbrev, game_id, stat):
	search_query = "SELECT {0} FROM teams_advanced.{1} WHERE game_id='{2}'".format(stat, team_abbrev, game_id)
	return get_one(search_query)