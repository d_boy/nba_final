from django import forms
from scripts.projector import *
from scripts.db_functions import *

TEAM_OPTIONS = (
	('all_teams', 'All'),
	('east_teams', 'Eastern Conference Only'),
	('west_teams', 'Western Conference Only'),
	('ATL', 'Atlanta Hawks'),
	('BKN', 'Brooklyn Nets'),
	('BOS', 'Boston Celtics'),
	('CHA', 'Charlotte Hornets'),
	('CHI', 'Chicago Bulls'),
	('CLE', 'Cleveland Cavaliers'),
	('DAL', 'Dallas Mavericks'),
	('DEN', 'Denver Nuggets'),
	('DET', 'Detroit Pistons'),
	('GSW', 'Golden State Warriors'),
	('HOU', 'Houston Rockets'),
	('IND', 'Indiana Pacers'),
	('LAC', 'Los Angeles Clippers'),
	('LAL', 'Los Angeles Lakers'),
	('MEM', 'Memphis Grizzlies'),
	('MIA', 'Miami Heat'),
	('MIL', 'Milwaukee Bucks'),
	('MIN', 'Minnesota Timberwolves'),
	('NOP', 'New Orleans Pelicans'),
	('NYK', 'New York Knicks'),
	('OKC', 'Oklahoma City Thunder'),
	('ORL', 'Orlando Magic'),
	('PHI', 'Philadelphia 76ers'),
	('PHX', 'Phoenix Suns'),
	('POR', 'Portland Trail Blazers'),
	('SAC', 'Sacramento Kings'),
	('SAS', 'San Antonio Spurs'),
	('TOR', 'Toronto Raptors'),
	('UTA', 'Utah Jazz'),
	('WAS', 'Washington Wizards'),
)


GAME_TYPES = (
	('reg_and_playoffs', 'Both'),
	('reg_season', 'Regular Season Only'),
	('playoffs_only', 'Playoffs Only'),
)

SEASONS = (
	('all_seasons', 'All'),
	('2015', '2015-16'),
	('2014', '2014-15'),
)

FAV_OR_DOG = (
	('all_fav_or_dog', 'All'),
	('fav_only', 'Favorites Only'),
	('dog_only', 'Underdogs Only'),
	('pickem_only', 'Pickems Only'),
)

HOME_OR_AWAY = (
	('both_home_or_away', 'Both'),
	('home_only', 'Home Games Only'),
	('away_only', 'Away Games Only'),
	)

VS_CONFERENCE = (
	('both_conferences', 'Both'),
	('east_only', 'Vs. Eastern Opponents Only'),
	('west_only', 'Vs. Western Opponents Only'),
	)

WON_OR_LOST = (
	('both_won_or_lost', 'Both'),
	('won_only', 'Wins Only'),
	('lost_only', 'Losses Only'),
	)

OVER_OR_UNDER = (
	('both_over_or_under', 'Both'),
	('over_only', 'Over Only'),
	('under_only', 'Under Only'),
	)

AXIS_OPTIONS = (
	('spread_diff', 'Spread Differential'),
	('total_diff', 'Total Differential'),
	('margin_of_victory', 'Margin of Victory'),
	('total', 'Total'),
	('points_scored', 'Points Scored'),
	('points_allowed', 'Points Allowed'),
	)

AXIS_OPTIONS_2 = (
	('total_diff', 'Total Differential'),
	('spread_diff', 'Spread Differential'),
	('margin_of_victory', 'Margin of Victory'),
	('total', 'Total'),
	('points_scored', 'Points Scored'),
	('points_allowed', 'Points Allowed'),
	)

COACH_OPTIONS = get_coach_list()
OFFICIAL_OPTIONS = get_official_list()

class FilterForm(forms.Form):
	
	u_filter_x_axis = forms.ChoiceField(choices=AXIS_OPTIONS, required=False, label="Choose X Axis")
	u_filter_y_axis = forms.ChoiceField(choices=AXIS_OPTIONS_2, required=False, label="Choose Y Axis")
	
	g_filter_teams = forms.ChoiceField(choices=TEAM_OPTIONS, required=False, label="Teams")
	g_filter_game_types = forms.ChoiceField(choices=GAME_TYPES, required=False, label="Game Type")
	g_filter_seasons = forms.ChoiceField(choices=SEASONS, required=False, label="Season")
	g_filter_days_rest = forms.BooleanField(required=False, label="Days Rest")
	g_filter_days_rest_min = forms.IntegerField(required=False, min_value=0, initial=0, label="Min")
	g_filter_days_rest_max = forms.IntegerField(required=False, min_value=0, initial=0, label="Max")
	g_filter_opp_days_rest = forms.BooleanField(required=False, label="Opponent Days Rest")
	g_filter_opp_days_rest_min = forms.IntegerField(required=False, min_value=0, initial=0, label="Min")
	g_filter_opp_days_rest_max = forms.IntegerField(required=False, min_value=0, initial=0, label="Max")
	g_filter_fav_or_dog = forms.ChoiceField(choices=FAV_OR_DOG, required=False, label="Favorite or Underdog")
	g_filter_coach = forms.ChoiceField(choices=COACH_OPTIONS, required=False, label="Coach")
	g_filter_official = forms.ChoiceField(choices=OFFICIAL_OPTIONS, required=False, label="Referee")
	g_filter_home_or_away = forms.ChoiceField(choices=HOME_OR_AWAY, required=False, label="Home or Away")
	# g_filter_vs_conference = forms.ChoiceField(choices=VS_CONFERENCE, required=False, label="Vs. Conference")
	g_filter_opp_teams = forms.ChoiceField(choices=TEAM_OPTIONS, required=False, label="Opponent")
	g_filter_spread = forms.BooleanField(required=False, label="Spread")
	g_filter_spread_min = forms.DecimalField(required=False, min_value=0, initial=0, label="Min")
	g_filter_spread_max = forms.DecimalField(required=False, min_value=0, initial=0, label="Max")
	g_filter_total = forms.BooleanField(required=False, label="Total")
	g_filter_total_min = forms.IntegerField(required=False, min_value=0, initial=0, label="Min")
	g_filter_total_max = forms.IntegerField(required=False, min_value=0, initial=0, label="Max")
	g_filter_won_or_lost = forms.ChoiceField(choices=WON_OR_LOST, required=False, label="Won or Lost")
	g_filter_won_or_lost_ats = forms.ChoiceField(choices=WON_OR_LOST, required=False, label="Won or Lost Against the Spread")
	g_filter_over_or_under = forms.ChoiceField(choices=OVER_OR_UNDER, required=False, label="Over or Under")
	g_filter_record = forms.BooleanField(required=False, label="Team Record")
	g_filter_win_pct_min = forms.DecimalField(required=False, min_value=0, initial=0, label="Win% Min", decimal_places=1)
	g_filter_win_pct_max = forms.DecimalField(required=False, max_value=1, initial=0, label="Win% Max", decimal_places=1)
	g_filter_opponent_record = forms.BooleanField(required=False, label="Opponent Record")
	g_filter_opponent_win_pct_min = forms.DecimalField(required=False, min_value=0, initial=0, label="Win% Min", decimal_places=1)
	g_filter_opponent_win_pct_max = forms.DecimalField(required=False, max_value=1, initial=0, label="Win% Max", decimal_places=1)
	
	filter_last_game_home_or_away = forms.ChoiceField(choices=HOME_OR_AWAY, required=False, label="Home or Away")
	# filter_last_game_vs_conference = forms.ChoiceField(choices=VS_CONFERENCE, required=False, label="Vs. Conference")
	filter_last_game_opp_team = forms.ChoiceField(choices=TEAM_OPTIONS, required=False, label="Opponent")
	filter_last_game_fav_or_dog = forms.ChoiceField(choices=FAV_OR_DOG, required=False, label="Favorite or Underdog")
	filter_last_game_won_or_lost = forms.ChoiceField(choices=WON_OR_LOST, required=False, label="W or L Outright")
	filter_last_game_won_or_lost_ats = forms.ChoiceField(choices=WON_OR_LOST, required=False, label="W or L Against Spread")
	filter_last_game_over_or_under = forms.ChoiceField(choices=OVER_OR_UNDER, required=False, label="Over or Under")
	filter_last_game_points_scored = forms.BooleanField(required=False, label="Points Scored")
	filter_last_game_points_scored_min = forms.IntegerField(required=False, min_value=0, initial=0, label="Min")
	filter_last_game_points_scored_max = forms.IntegerField(required=False, min_value=0, initial=0, label="Max")
	filter_last_game_points_allowed = forms.BooleanField(required=False, label="Points Allowed")
	filter_last_game_points_allowed_min = forms.IntegerField(required=False, min_value=0, initial=0, label="Min")
	filter_last_game_points_allowed_max = forms.IntegerField(required=False, min_value=0, initial=0, label="Max")
	filter_last_game_margin_of_victory = forms.BooleanField(required=False, label="Margin of Victory")
	filter_last_game_margin_of_victory_min = forms.IntegerField(required=False, initial=0, label="Min")
	filter_last_game_margin_of_victory_max = forms.IntegerField(required=False, initial=0, label="Max")
	filter_last_game_spread_difference = forms.BooleanField(required=False, label="Spread Difference")
	filter_last_game_spread_difference_min = forms.DecimalField(required=False, initial=0, label="Min")
	filter_last_game_spread_difference_max = forms.DecimalField(required=False, initial=0, label="Max")
	filter_last_game_total_difference = forms.BooleanField(required=False, label="Total Difference")
	filter_last_game_total_difference_min = forms.DecimalField(required=False, initial=0, label="Min")
	filter_last_game_total_difference_max = forms.DecimalField(required=False, initial=0, label="Max")
	filter_last_game_opponent_record = forms.BooleanField(required=False, label="Opponent Record")
	filter_last_game_opponent_win_pct_min = forms.DecimalField(required=False, min_value=0, initial=0, label="Win% Min")
	filter_last_game_opponent_win_pct_max = forms.DecimalField(required=False, max_value=1, initial=0, label="Win% Max")
	# filter_last_game_distance_traveled = forms.BooleanField(required=False, label="Miles Traveled (~17 is avg)")
	# filter_last_game_distance_traveled_min = forms.DecimalField(required=False, min_value=0, initial=0, label="Min")
	# filter_last_game_distance_traveled_max = forms.DecimalField(required=False, min_value=0, initial=0, label="Max")

	def group1(self):
		group = [self['u_filter_x_axis'], self['u_filter_y_axis']]
		return group

	def group2(self):
		name_arr = [
			'g_filter_teams',
			'g_filter_game_types',
			'g_filter_seasons',
			'g_filter_days_rest',
			'g_filter_days_rest_min',
			'g_filter_days_rest_max',
			'g_filter_opp_days_rest',
			'g_filter_opp_days_rest_min',
			'g_filter_opp_days_rest_max',
			'g_filter_fav_or_dog',
			'g_filter_coach',
			'g_filter_official',
			'g_filter_home_or_away',
			'g_filter_opp_teams',
			'g_filter_spread',
			'g_filter_spread_min',
			'g_filter_spread_max',
			'g_filter_total',
			'g_filter_total_min',
			'g_filter_total_max',
			'g_filter_won_or_lost',
			'g_filter_won_or_lost_ats',
			'g_filter_over_or_under',
			'g_filter_record',
			'g_filter_win_pct_min',
			'g_filter_win_pct_max',
			'g_filter_opponent_record',
			'g_filter_opponent_win_pct_min',
			'g_filter_opponent_win_pct_max'
		]
		group = []
		for name in name_arr:
			group.append(self[name])
		return group

	def group3(self):
		name_arr = [
			'filter_last_game_home_or_away',
			'filter_last_game_opp_team',
			'filter_last_game_fav_or_dog',
			'filter_last_game_won_or_lost',
			'filter_last_game_won_or_lost_ats',
			'filter_last_game_over_or_under',
			'filter_last_game_points_scored',
			'filter_last_game_points_scored_min',
			'filter_last_game_points_scored_max',
			'filter_last_game_points_allowed',
			'filter_last_game_points_allowed_min',
			'filter_last_game_points_allowed_max',
			'filter_last_game_margin_of_victory',
			'filter_last_game_margin_of_victory_min',
			'filter_last_game_margin_of_victory_max',
			'filter_last_game_spread_difference',
			'filter_last_game_spread_difference_min',
			'filter_last_game_spread_difference_max',
			'filter_last_game_total_difference',
			'filter_last_game_total_difference_min',
			'filter_last_game_total_difference_max',
			'filter_last_game_opponent_record',
			'filter_last_game_opponent_win_pct_min',
			'filter_last_game_opponent_win_pct_max'
			# 'filter_last_game_distance_traveled',
			# 'filter_last_game_distance_traveled_min',
			# 'filter_last_game_distance_traveled_max'
		]
		group = []
		for name in name_arr:
			group.append(self[name])
		return group

	def post(self, request):
		post_parser(request)
		return True

	def form_valid(self, form):
		print("ASSCHEESE")
		return True

