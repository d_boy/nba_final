function updateData(result) {
    var data = result;
    var dataSize = 0;
    var xAvg = 0;
    var yAvg = 0;
    var rsquared = 0;
    if (data.length == 2) {
        var axes = data[0];
        dataSize = 0;
        var x_axis = axes['x_axis'];
        var y_axis = axes['y_axis'];
    } else {
        var axes = data[data.length - 2];
        //data.pop();
        dataSize = data.length - 2;
        var x_axis = axes['x_axis'];
        var y_axis = axes['y_axis'];
        var stats = data[data.length - 1];
        xAvg = stats['x_avg'];
        yAvg = stats['y_avg'];
        rsquared = stats['rsquared'];
        data.pop();
        data.pop();
    }

    var xAbove = 0;
    var xBelow = 0;
    var yAbove = 0;
    var yBelow = 0;

    for (var x = 0; x < dataSize; x++) {
        if (data[x]['x_value'] > 0) {
            xAbove++;
        } else if (data[x]['x_value'] < 0) {
            xBelow++;
        }
        if (data[x]['y_value'] > 0) {
            yAbove++;
        } else if (data[x]['y_value'] < 0) {
            yBelow++;
        }
    }

    d3.select("#sampleSize").text("Sample Size: " + dataSize);
    d3.select("#sampleInfoX").text(x_axis + " > 0: " + xAbove + ", " + x_axis + " < 0: " + xBelow);
    d3.select("#sampleMeanX").text("Mean " + x_axis + ": " + xAvg);
    d3.select("#sampleInfoY").text(y_axis + " > 0: " + yAbove + ", " + y_axis + " < 0: " + yBelow);
    d3.select("#sampleMeanY").text("Mean " + y_axis + ": " + yAvg);


    $("input[type=checkbox]").change(function() {
        min = $("#" + this.id + "_min");
        max = $("#" + this.id + "_max");
        minParent = min.parent();
        maxParent = max.parent();
        if (this.checked) {
            minParent.css('display', 'inline-block');
            maxParent.css('display', 'inline-block');
            min.css('width', '110px');
            min.css('margin-right', '3px');
            max.css('width', '110px');
        } else {
            minParent.css('display', 'none');
            maxParent.css('display', 'none');
        }
    });

    d3.select("#resetFilters")
        .on("click", function() {

        });

    var margin = {top: 20, right: 15, bottom: 60, left: 60}
        , width = 960 - margin.left - margin.right
        , height = 500 - margin.top - margin.bottom;

    var x = d3.scaleLinear()
        .domain([d3.min(data, function(d) { return d.x_value - 5; }), d3.max(data, function(d) { return d.x_value + 5; })])
        .range([ 0, width ]);

    var y = d3.scaleLinear()
        .domain([d3.min(data, function(d) { return d.y_value - 5; }), d3.max(data, function(d) { return d.y_value + 5; })])
        .range([ height, 0 ]);

    var chart = d3.select('#content')
        .append('svg:svg')
        .attr('width', width + margin.right + margin.left)
        .attr('height', height + margin.top + margin.bottom)
        .attr('class', 'chart')

    var main = chart.append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'main')

    // draw the x axis
    var xAxis = d3.axisBottom()
        .scale(x);

    main.append('g')
        .attr('transform', 'translate(0,' + height + ')')
        .call(xAxis);

    // draw the y axis
    var yAxis = d3.axisLeft()
        .scale(y);

    main.append('g')
        .attr('transform', 'translate(0,0)')
        .call(yAxis);

    var g = main.append("svg:g");

    var div = d3.select("#content").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    if (dataSize > 0) {
        g.selectAll("scatter-dots")
            .data(data)
            .enter().append("svg:circle")
            .attr("class", "data-circle")
            .attr("cx", function (d,i) { return x(d.x_value); } )
            .attr("cy", function (d) { return y(d.y_value); } )
            .attr("r", 8)
            .attr("xlink:href", function(d) { return d.hyperlink; })
            .on("click", function(d) {
                var newWindow = window.open(d.hyperlink);
            })
            .on("mouseover", function(d) {
                div.transition()
                    .duration(200)
                    .style("opacity", .99);
                div.html(d.description + "<br/>"  + d.projection + "<br/>" + d.actual)
                    .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY - 45) + "px");
            })
            .on("mouseout", function(d) {
                div.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    }

    g.append("text")
        .attr("transform",
            "translate(" + (width/2) + " ," +
            (height + margin.top + 20) + ")")
        .style("text-anchor", "middle")
        .style("font-size", "14px")
        .text(x_axis);

    g.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - margin.left)
        .attr("x",0 - (height / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .style("font-size", "14px")
        .text(y_axis);
}