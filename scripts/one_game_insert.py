# #Each function takes in data from a single game and inserts it to the proper table.

# from __future__ import print_function
# from nba_py import game
# import psycopg2
# import pandas
# from util import *
# from db_functions import *
# from coaches import *

# def parse_info(game_id, season, playoffs):
# 	value_arr = [season, game_id]
# 	table_arr = ["season", "game_id"]

# 	info = game.BoxscoreSummary(game_id)
# 	game_summary = info.game_summary()
# 	game_info = info.game_info()
# 	officials = info.officials()
# 	line_score = info.line_score()
# 	last_meeting = info.last_meeting()
# 	box = game.Boxscore(game_id)

# 	game_date = str(game_summary['GAME_DATE_EST'][0])[:10]
# 	away_team_id = str(game_summary['VISITOR_TEAM_ID'][0])
# 	home_team_id = str(game_summary['HOME_TEAM_ID'][0])

# 	if (str(line_score['TEAM_ID'][0]) == away_team_id):
# 		away_index = 0
# 		home_index = 1
# 	else:
# 		away_index = 1
# 		home_index = 0

# 	away_team_abbrev = str(line_score['TEAM_ABBREVIATION'][away_index])
# 	away_team_city = str(line_score['TEAM_CITY_NAME'][away_index]).replace(" ", "")
# 	away_team_nickname = str(line_score['TEAM_NICKNAME'][away_index]).replace(" ", "")
# 	#away_team_coach = find_coach(game_id, away_team_abbrev, season + 1)
	
# 	home_team_abbrev = str(line_score['TEAM_ABBREVIATION'][home_index])
# 	home_team_city = str(line_score['TEAM_CITY_NAME'][home_index]).replace(" ", "")
# 	home_team_nickname = str(line_score['TEAM_NICKNAME'][home_index]).replace(" ", "")
# 	#home_team_coach = find_coach(game_id, home_team_abbrev, season + 1)

# 	official_1_id = str(officials['OFFICIAL_ID'][0])
# 	official_2_id = str(officials['OFFICIAL_ID'][1])
	
# 	if len(officials['OFFICIAL_ID']) > 3:
# 		official_3_id = str(officials['OFFICIAL_ID'][2])
# 		official_4_id = str(officials['OFFICIAL_ID'][3])
# 	elif (len(officials['OFFICIAL_ID']) > 2):
# 		official_3_id = str(officials['OFFICIAL_ID'][2])
# 		official_4_id = None
# 	else:
# 		official_3_id = None
# 		official_4_id = None

# 	last_meeting_id = str(last_meeting['LAST_GAME_ID'][0])

# 	if (getInt(line_score['PTS'][home_index]) > getInt(line_score['PTS'][away_index])):
# 		winning_team_id = home_team_id
# 		winning_team_abbrev = home_team_abbrev
# 		losing_team_id = away_team_id
# 		losing_team_abbrev = away_team_abbrev
# 		winning_team_score = getInt(line_score['PTS'][home_index])
# 		losing_team_score = getInt(line_score['PTS'][away_index])
# 		home_won_or_lost = "W"
# 		away_won_or_lost = "L"
# 	else:
# 		winning_team_id = away_team_id
# 		winning_team_abbrev = away_team_abbrev
# 		losing_team_id = home_team_id
# 		losing_team_abbrev = home_team_abbrev
# 		winning_team_score = getInt(line_score['PTS'][away_index])
# 		losing_team_score = getInt(line_score['PTS'][home_index])
# 		away_won_or_lost = "W"
# 		home_won_or_lost = "L"


# 	away_team_score = getInt(line_score['PTS'][away_index])
# 	home_team_score = getInt(line_score['PTS'][home_index])
# 	total_score = away_team_score + home_team_score

# 	attendance = getInt(game_info['ATTENDANCE'])
# 	home_team_pts_1q = getInt(line_score['PTS_QTR1'][home_index])
# 	home_team_pts_2q = getInt(line_score['PTS_QTR2'][home_index])
# 	home_team_pts_3q = getInt(line_score['PTS_QTR3'][home_index])
# 	home_team_pts_4q = getInt(line_score['PTS_QTR4'][home_index])
# 	home_team_pts_ot1 = getInt(line_score['PTS_OT1'][home_index])
# 	home_team_pts_ot2 = getInt(line_score['PTS_OT2'][home_index])
# 	home_team_pts_ot3 = getInt(line_score['PTS_OT3'][home_index])
# 	home_team_pts_ot4 = getInt(line_score['PTS_OT4'][home_index])
# 	home_team_pts_ot5 = getInt(line_score['PTS_OT5'][home_index])
# 	away_team_pts_1q = getInt(line_score['PTS_QTR1'][away_index])
# 	away_team_pts_2q = getInt(line_score['PTS_QTR2'][away_index])
# 	away_team_pts_3q = getInt(line_score['PTS_QTR3'][away_index])
# 	away_team_pts_4q = getInt(line_score['PTS_QTR4'][away_index])
# 	away_team_pts_ot1 = getInt(line_score['PTS_OT1'][away_index])
# 	away_team_pts_ot2 = getInt(line_score['PTS_OT2'][away_index])
# 	away_team_pts_ot3 = getInt(line_score['PTS_OT3'][away_index])
# 	away_team_pts_ot4 = getInt(line_score['PTS_OT4'][away_index])
# 	away_team_pts_ot5 = getInt(line_score['PTS_OT5'][away_index])

# 	overtime = 0

# 	if (home_team_pts_ot1 == 0 and away_team_pts_ot1 == 0):
# 		overtime = 0
# 	elif (home_team_pts_ot2 == 0 and away_team_pts_ot2 == 0 ):
# 		overtime = 1
# 	elif (home_team_pts_ot3 == 0 and away_team_pts_ot3 == 0 ):
# 		overtime = 2
# 	elif (home_team_pts_ot4 == 0 and away_team_pts_ot4 == 0 ):
# 		overtime = 3
# 	elif (home_team_pts_ot5 == 0 and away_team_pts_ot5 == 0 ):
# 		overtime = 4

# 	game_num_away = find_game_num(away_team_abbrev, season) + 1
# 	game_num_home = find_game_num(home_team_abbrev, season) + 1

# 	away_team_coach = find_coach(game_id, away_team_abbrev, season + 1, game_num_away, playoffs)
# 	home_team_coach = find_coach(game_id, home_team_abbrev, season + 1, game_num_home, playoffs)

# 	value_arr.extend([game_date, playoffs, away_team_id, home_team_id, away_team_abbrev, home_team_abbrev, away_team_coach, home_team_coach, official_1_id, official_2_id, official_3_id, official_4_id, last_meeting_id, winning_team_id, losing_team_id, winning_team_abbrev, losing_team_abbrev, away_team_score, home_team_score, winning_team_score, losing_team_score, overtime, attendance, away_team_pts_1q, away_team_pts_2q, away_team_pts_3q, away_team_pts_4q, away_team_pts_ot1, away_team_pts_ot2, away_team_pts_ot3, away_team_pts_ot4, home_team_pts_1q, home_team_pts_2q, home_team_pts_3q, home_team_pts_4q, home_team_pts_ot1, home_team_pts_ot2, home_team_pts_ot3, home_team_pts_ot4, total_score])
# 	table_arr.extend(["game_date", "playoff_game", "away_team_id", "home_team_id", "away_team_abbrev", "home_team_abbrev", "away_team_coach", "home_team_coach", "official_1_id", "official_2_id", "official_3_id", "official_4_id", "last_meeting_id", "winning_team_id", "losing_team_id", "winning_team_abbrev", "losing_team_abbrev", "away_team_score", "home_team_score", "winning_team_score", "losing_team_score", "overtime", "attendance", "away_team_pts_1q", "away_team_pts_2q", "away_team_pts_3q", "away_team_pts_4q", "away_team_pts_ot1", "away_team_pts_ot2", "away_team_pts_ot3", "away_team_pts_ot4", "home_team_pts_1q", "home_team_pts_2q", "home_team_pts_3q", "home_team_pts_4q", "home_team_pts_ot1", "home_team_pts_ot2", "home_team_pts_ot3", "home_team_pts_ot4", "total_score"])

# 	suffix = "season_%s" % season
# 	insert_entry("games_info", suffix, table_arr, value_arr)

# 	for x in range(0, len(officials['OFFICIAL_ID'])):
# 		official_id = str(officials['OFFICIAL_ID'][x])
# 		first_name = str(officials['FIRST_NAME'][x])
# 		last_name = str(officials['LAST_NAME'][x])
# 		table_ref = "%s_%s" % (first_name, last_name)
# 		insert_official(official_id, first_name, last_name, table_ref)
			
# 	insert_team(away_team_id, away_team_abbrev, away_team_city, away_team_nickname)
# 	create_team_table_info(away_team_abbrev)

# 	insert_team(home_team_id, home_team_abbrev, home_team_city, home_team_nickname)
# 	create_team_table_info(home_team_abbrev)

# 	away_team_arr = [season, game_id, game_date, playoffs, game_num_away, away_team_id, away_team_coach, "away", home_team_id, home_team_abbrev, official_1_id, official_2_id, official_3_id, official_4_id, last_meeting_id, away_won_or_lost, away_team_score, home_team_score, overtime, attendance, away_team_pts_1q, away_team_pts_2q, away_team_pts_3q, away_team_pts_4q, away_team_pts_ot1, away_team_pts_ot2, away_team_pts_ot3, away_team_pts_ot4, home_team_pts_1q, home_team_pts_2q, home_team_pts_3q, home_team_pts_4q, home_team_pts_ot1, home_team_pts_ot2, home_team_pts_ot3, home_team_pts_ot4]
# 	team_table_arr = ["season", "game_id", "game_date", "playoff_game", "game_num", "team_id", "coach_name", "home_or_away", "opp_team_id", "opp_team_abbrev", "official_1_id", "official_2_id", "official_3_id", "official_4_id", "last_meeting_id", "won_or_lost", "team_score", "opp_team_score", "overtime", "attendance", "team_pts_1q", "team_pts_2q", "team_pts_3q", "team_pts_4q", "team_pts_ot1", "team_pts_ot2", "team_pts_ot3", "team_pts_ot4", "opp_team_pts_1q", "opp_team_pts_2q", "opp_team_pts_3q", "opp_team_pts_4q", "opp_team_pts_ot1", "opp_team_pts_ot2", "opp_team_pts_ot3", "opp_team_pts_ot4"]
# 	home_team_arr = [season, game_id, game_date, playoffs, game_num_home, home_team_id, home_team_coach, "home", away_team_id, away_team_abbrev, official_1_id, official_2_id, official_3_id, official_4_id, last_meeting_id, home_won_or_lost, home_team_score, away_team_score, overtime, attendance, home_team_pts_1q, home_team_pts_2q, home_team_pts_3q, home_team_pts_4q, home_team_pts_ot1, home_team_pts_ot2, home_team_pts_ot3, home_team_pts_ot4, away_team_pts_1q, away_team_pts_2q, away_team_pts_3q, away_team_pts_4q, away_team_pts_ot1, away_team_pts_ot2, away_team_pts_ot3, away_team_pts_ot4]

# 	insert_entry("teams_info", away_team_abbrev, team_table_arr, away_team_arr)
# 	insert_entry("teams_info", home_team_abbrev, team_table_arr, home_team_arr)

# 	insert_entry("coaches", "coaches", ["coach_name"], [away_team_coach])
# 	insert_entry("coaches", "coaches", ["coach_name"], [home_team_coach])


# def parse_basic(game_id, season):
	
# 	box = game.Boxscore(game_id)
# 	player_stats = box.player_stats()
# 	team_stats = box.team_stats()
# 	info = game.BoxscoreSummary(game_id)
# 	game_summary = info.game_summary()
# 	line_score = info.line_score()

# 	for x in range(0, player_stats['PLAYER_NAME'].count()):		
# 		value_arr = [season, game_id]
# 		table_arr = ["season", "game_id"]

# 		playoff_game = 0
# 		team_id = str(player_stats['TEAM_ID'][x])
# 		team_abbrev = player_stats['TEAM_ABBREVIATION'][x]
# 		away_team_id = str(game_summary['VISITOR_TEAM_ID'][0])
# 		home_team_id = str(game_summary['HOME_TEAM_ID'][0])

# 		if (str(line_score['TEAM_ID'][0]) == away_team_id):
# 			away_index = 0
# 			home_index = 1
# 		else:
# 			away_index = 1
# 			home_index = 0

# 		away_team_abbrev = str(line_score['TEAM_ABBREVIATION'][away_index])
# 		home_team_abbrev = str(line_score['TEAM_ABBREVIATION'][home_index])
# 		player_id = player_stats['PLAYER_ID'][x]
# 		player_name = player_stats['PLAYER_NAME'][x].replace("'", "")
# 		player_name = player_name.replace("-", "")
# 		player_name = player_name.replace(".", "")
# 		start_position = player_stats['START_POSITION'][x]
# 		comment = player_stats['COMMENT'][x].replace("'", "")
# 		minutes = convertMinutes(player_stats['MIN'][x])
# 		fgm = getInt(player_stats['FGM'][x])
# 		fga = getInt(player_stats['FGA'][x])
# 		fg_pct = getDouble(player_stats['FG_PCT'][x])
# 		threepm = getInt(player_stats['FG3M'][x])
# 		threepa = getInt(player_stats['FG3A'][x])
# 		threep_pct = getDouble(player_stats['FG3_PCT'][x])
# 		ftm = getInt(player_stats['FTM'][x])
# 		fta = getInt(player_stats['FTA'][x])
# 		ft_pct = getDouble(player_stats['FT_PCT'][x])
# 		o_reb = getInt(player_stats['OREB'][x])
# 		d_reb = getInt(player_stats['DREB'][x])
# 		reb = getInt(player_stats['REB'][x])
# 		assist = getInt(player_stats['AST'][x])
# 		stl = getInt(player_stats['STL'][x])
# 		blk = getInt(player_stats['BLK'][x])
# 		tov = getInt(player_stats['TO'][x])
# 		pf = getInt(player_stats['PF'][x])
# 		pts = getInt(player_stats['PTS'][x])
# 		plus_minus = getInt(player_stats['PLUS_MINUS'][x])

# 		name_arr = str(player_name).split(" ")
# 		first_name = name_arr[0]
# 		if len(name_arr) < 2:
# 			last_name = ""
# 		else:
# 			last_name = name_arr[1]
# 		table_ref = "%s_%s" % (first_name, last_name)

# 		value_arr.extend([team_id, team_abbrev, away_team_id, away_team_abbrev, home_team_id, home_team_abbrev, player_id, player_name, start_position, comment, minutes, fgm, fga, fg_pct, threepm, threepa, threep_pct, ftm, fta, ft_pct, o_reb, d_reb, reb, assist, tov, stl, blk, pf, pts, plus_minus])
# 		table_arr.extend(["team_id", "team_abbrev", "away_team_id", "away_team_abbrev", "home_team_id", "home_team_abbrev", "player_id", "player_name", "start_position", "comment", "min", "fgm", "fga", "fg_pct", "threepm", "threepa", "threep_pct", "ftm", "fta", "ft_pct", "o_reb", "d_reb", "reb", "assist", "tov", "stl", "blk", "pf", "pts", "plus_minus"])

# 		insert_player(player_id, first_name, last_name, table_ref)
# 		create_player_table_basic(table_ref)
	
# 		if (team_id == away_team_id):
# 			home_or_away = "away"
			
# 			if (team_id == str(team_stats['TEAM_ID'][0])):
# 				index = 0
# 				opp_index = 1
# 			else:
# 				index = 1
# 				opp_index = 0

# 			my_team_score = getInt(team_stats['PTS'][index])
# 			opp_team_score = getInt(team_stats['PTS'][opp_index])
# 			team_fgm = getInt(team_stats['FGM'][index])
# 			team_fga = getInt(team_stats['FGA'][index])
# 			team_fg_pct = getDouble(team_stats['FG_PCT'][index])
# 			team_threepm = getInt(team_stats['FG3M'][index])
# 			team_threepa = getInt(team_stats['FG3A'][index])
# 			team_threep_pct = getDouble(team_stats['FG3_PCT'][index])
# 			team_ftm = getInt(team_stats['FTM'][index])
# 			team_fta = getInt(team_stats['FTA'][index])
# 			team_ft_pct = getDouble(team_stats['FT_PCT'][index])
# 			team_o_reb = getInt(team_stats['OREB'][index])
# 			team_d_reb = getInt(team_stats['DREB'][index])
# 			team_reb = getInt(team_stats['REB'][index])
# 			team_assist = getInt(team_stats['AST'][index])
# 			team_stl = getInt(team_stats['STL'][index])
# 			team_blk = getInt(team_stats['BLK'][index])
# 			team_tov = getInt(team_stats['TO'][index])
# 			team_pf = getInt(team_stats['PF'][index])
# 			team_pts = getInt(team_stats['PTS'][index])
# 			team_plus_minus = getInt(team_stats['PLUS_MINUS'][index])
# 		else:
# 			home_or_away = "home"

# 			if (team_id == str(team_stats['TEAM_ID'][0])):
# 				index = 0
# 				opp_index = 1
# 			else:
# 				index = 1
# 				opp_index = 0

# 			my_team_score = getInt(team_stats['PTS'][index])
# 			opp_team_score = getInt(team_stats['PTS'][opp_index])
# 			team_fgm = getInt(team_stats['FGM'][index])
# 			team_fga = getInt(team_stats['FGA'][index])
# 			team_fg_pct = getDouble(team_stats['FG_PCT'][index])
# 			team_threepm = getInt(team_stats['FG3M'][index])
# 			team_threepa = getInt(team_stats['FG3A'][index])
# 			team_threep_pct = getDouble(team_stats['FG3_PCT'][index])
# 			team_ftm = getInt(team_stats['FTM'][index])
# 			team_fta = getInt(team_stats['FTA'][index])
# 			team_ft_pct = getDouble(team_stats['FT_PCT'][index])
# 			team_o_reb = getInt(team_stats['OREB'][index])
# 			team_d_reb = getInt(team_stats['DREB'][index])
# 			team_reb = getInt(team_stats['REB'][index])
# 			team_assist = getInt(team_stats['AST'][index])
# 			team_stl = getInt(team_stats['STL'][index])
# 			team_blk = getInt(team_stats['BLK'][index])
# 			team_tov = getInt(team_stats['TO'][index])
# 			team_pf = getInt(team_stats['PF'][index])
# 			team_pts = getInt(team_stats['PTS'][index])
# 			team_plus_minus = getInt(team_stats['PLUS_MINUS'][index])

# 		if (my_team_score > opp_team_score):
# 			won_or_lost = "W"
# 		else:
# 			won_or_lost = "L"

# 		value_arr.insert(2, playoff_game)
# 		table_arr.insert(2, "playoff_game")
# 		value_arr.insert(9, my_team_score)
# 		value_arr.insert(10, opp_team_score)
# 		value_arr.insert(11, home_or_away)
# 		value_arr.insert(12, won_or_lost)
# 		table_arr.insert(9, "my_team_score")
# 		table_arr.insert(10, "opp_team_score")
# 		table_arr.insert(11, "home_or_away")
# 		table_arr.insert(12, "won_or_lost")

# 		insert_entry("players_basic", table_ref, table_arr, value_arr)

# 		create_team_table_basic(team_abbrev)
# 		team_table_arr = ["season", "game_id", "team_id", "team_abbrev", "away_team_id", "away_team_abbrev", "home_team_id", "home_team_abbrev", "team_score", "opp_team_score", "home_or_away", "won_or_lost", "fgm", "fga", "fg_pct", "threepm", "threepa", "threep_pct", "ftm", "fta", "ft_pct", "o_reb", "d_reb", "reb", "assist", "tov", "stl", "blk", "pf", "pts", "plus_minus"]
# 		team_value_arr = [season, game_id, team_id, team_abbrev, away_team_id, away_team_abbrev, home_team_id, home_team_abbrev, my_team_score, opp_team_score, home_or_away, won_or_lost, team_fgm, team_fga, team_fg_pct, team_threepm, team_threepa, team_threep_pct, team_ftm, team_fta, team_ft_pct, team_o_reb, team_d_reb, team_reb, team_assist, team_tov, team_stl, team_blk, team_pf, team_pts, team_plus_minus]	
# 		insert_entry("teams_basic", team_abbrev, team_table_arr, team_value_arr)
		

	

# def parse_advanced(game_id, season):
# 	box_adv = game.BoxscoreAdvanced(game_id)
# 	player_stats = box_adv.sql_players_advanced()
# 	team_stats = box_adv.sql_team_advanced()

# 	for x in range(0, player_stats['PLAYER_NAME'].count()):		
# 		player_id = player_stats['PLAYER_ID'][x]
# 		player_name = player_stats['PLAYER_NAME'][x].replace("'", "")
# 		player_name = player_name.replace("-", "")
# 		player_name = player_name.replace(".", "")
# 		table_ref = get_table_ref(player_name)

# 		minutes = convertMinutes(player_stats['MIN'][x])
# 		o_rtg = getDouble(player_stats['OFF_RATING'][x])
# 		d_rtg = getDouble(player_stats['DEF_RATING'][x])
# 		net_rtg = getDouble(player_stats['NET_RATING'][x])
# 		ast_pct = getDouble(player_stats['AST_PCT'][x])
# 		ast_to_ratio = get_ast_to_ratio(table_ref, game_id, "players_basic")
# 		ast_ratio = getDouble(player_stats['AST_RATIO'][x])
# 		o_reb_pct = getDouble(player_stats['OREB_PCT'][x])
# 		d_reb_pct = getDouble(player_stats['DREB_PCT'][x])
# 		reb_pct = getDouble(player_stats['REB_PCT'][x])
# 		tov_pct = getDouble(player_stats['TM_TOV_PCT'][x])
# 		efg_pct = getDouble(player_stats['EFG_PCT'][x])
# 		ts_pct = getDouble(player_stats['TS_PCT'][x])
# 		usage_pct = getDouble(player_stats['USG_PCT'][x])
# 		pace = getDouble(player_stats['PACE'][x])
# 		pie = getDouble(player_stats['PIE'][x])
		
# 		if (x == 0 or x == 18):

# 			team_id = str(player_stats['TEAM_ID'][x])

# 			if (team_id == str(team_stats['TEAM_ID'][0])):
# 				index = 0
# 			else:
# 				index = 1

# 			team_abbrev = str(team_stats['TEAM_ABBREVIATION'][index])
# 			#team_name = str(team_stats['TEAM_NAME'][index])
# 			team_minutes = 240
# 			team_o_rtg = getDouble(team_stats['OFF_RATING'][index])
# 			team_d_rtg = getDouble(team_stats['DEF_RATING'][index])
# 			team_net_rtg = getDouble(team_stats['NET_RATING'][index])
# 			team_ast_pct = getDouble(team_stats['AST_PCT'][index])
# 			team_ast_to_ratio = get_ast_to_ratio(team_abbrev, game_id, "teams_basic")
# 			team_ast_ratio = getDouble(team_stats['AST_RATIO'][index])
# 			team_o_reb_pct = getDouble(team_stats['OREB_PCT'][index])
# 			team_d_reb_pct = getDouble(team_stats['DREB_PCT'][index])
# 			team_reb_pct = getDouble(team_stats['REB_PCT'][index])
# 			team_tov_pct = getDouble(team_stats['TM_TOV_PCT'][index])
# 			team_efg_pct = getDouble(team_stats['EFG_PCT'][index])
# 			team_ts_pct = getDouble(team_stats['TS_PCT'][index])
# 			team_usage_pct = getDouble(team_stats['USG_PCT'][index])
# 			team_pace = getDouble(team_stats['PACE'][index]) 
# 			team_pie = getDouble(team_stats['PIE'][index])

# 			table_team_arr = ["season", "game_id", "team_id", "team_abbrev", "minutes", "o_rtg", "d_rtg", "net_rtg", "ast_pct", "ast_to_ratio", "ast_ratio", "o_reb_pct", "d_reb_pct", "reb_pct", "tov_pct", "efg_pct", "ts_pct", "usage_pct", "pace", "pie"]
# 			team_vals = [season, game_id, team_id, team_abbrev, team_minutes, team_o_rtg, team_d_rtg, team_net_rtg, team_ast_pct, team_ast_to_ratio, team_ast_ratio, team_o_reb_pct, team_d_reb_pct, team_reb_pct, team_tov_pct, team_efg_pct, team_ts_pct, team_usage_pct, team_pace, team_pie]
# 			create_team_table_advanced(team_abbrev)
# 			insert_entry("teams_advanced", team_abbrev, table_team_arr, team_vals)


# 		table_player_arr = ["season", "game_id", "player_id", "player_name", "minutes", "o_rtg", "d_rtg", "net_rtg", "ast_pct", "ast_to_ratio", "ast_ratio", "o_reb_pct", "d_reb_pct", "reb_pct", "tov_pct", "efg_pct", "ts_pct", "usage_pct", "pace", "pie"]
# 		player_vals = [season, game_id, player_id, player_name, minutes, o_rtg, d_rtg, net_rtg, ast_pct, ast_to_ratio, ast_ratio, o_reb_pct, d_reb_pct, reb_pct, tov_pct, efg_pct, ts_pct, usage_pct, pace, pie]
# 		create_player_table_advanced(table_ref)
# 		insert_entry("players_advanced", table_ref, table_player_arr, player_vals)


# def parse_tracking(game_id, season):
# 	pt = game.PlayerTracking(game_id)
# 	tracking = pt.info()

# 	for x in range(0, tracking['PLAYER_NAME'].count()):		
# 		player_id = tracking['PLAYER_ID'][x]
# 		player_name = tracking['PLAYER_NAME'][x].replace("'", "")
# 		player_name = player_name.replace("-", "")
# 		player_name = player_name.replace(".", "")
# 		table_ref = get_table_ref(player_name)

# 		minutes = convertMinutes(tracking['MIN'][x])
# 		dist_trav = getDouble(tracking['DIST'][x])
# 		avg_speed = getDouble(tracking['SPD'][x])
# 		touches = getInt(tracking['TCHS'][x])
# 		passes = getInt(tracking['PASS'][x])
# 		assists = getInt(tracking['AST'][x])
# 		sec_ast = getInt(tracking['SAST'][x])
# 		ft_ast = getInt(tracking['FTAST'][x])
# 		d_atrim_fgm = getInt(tracking['DFGM'][x])
# 		d_atrim_fga = getInt(tracking['DFGA'][x])
# 		d_atrim_fg_pct = getDouble(tracking['DFG_PCT'][x])
# 		oreb_chance = getInt(tracking['ORBC'][x])
# 		dreb_chance = getInt(tracking['DRBC'][x])
# 		reb_chance = getInt(tracking['RBC'][x])
# 		fg_pct = getDouble(tracking['FG_PCT'][x])
# 		cfgm = getInt(tracking['CFGM'][x])
# 		cfga = getInt(tracking['CFGA'][x])
# 		cfg_pct = getDouble(tracking['CFG_PCT'][x])
# 		ufgm = getInt(tracking['UFGM'][x])
# 		ufga = getInt(tracking['UFGA'][x])
# 		ufg_pct = getDouble(tracking['UFG_PCT'][x])

# 		if x > 4:
# 			if (tracking['TEAM_ID'][x] != tracking['TEAM_ID'][x - 1]):
# 				team_1_id = tracking['TEAM_ID'][x - 1]
# 				team_1_abbrev = tracking['TEAM_ABBREVIATION'][x - 1]
# 				team_1_dist = sum(tracking['DIST'][:x])
# 				team_1_touches = sum(tracking['TCHS'][:x])
# 				team_1_passes = sum(tracking['PASS'][:x])
# 				team_1_assist = sum(tracking['AST'][:x])
# 				team_1_sec_ast = sum(tracking['SAST'][:x])
# 				team_1_ft_ast = sum(tracking['FTAST'][:x])
# 				team_1_d_atrim_fgm = sum(tracking['DFGM'][:x])
# 				team_1_d_atrim_fga = sum(tracking['DFGA'][:x])
# 				team_1_d_atrim_fg_pct = calc_pct(getDouble(team_1_d_atrim_fgm), getDouble(team_1_d_atrim_fga))
# 				team_1_oreb_chance = sum(tracking['ORBC'][:x])
# 				team_1_dreb_chance = sum(tracking['DRBC'][:x])
# 				team_1_reb_chance = sum(tracking['RBC'][:x])
# 				team_1_cfgm =  sum(tracking['CFGM'][:x])
# 				team_1_cfga =  sum(tracking['CFGA'][:x])
# 				team_1_cfg_pct = calc_pct(getDouble(team_1_cfgm), getDouble(team_1_cfga))
# 				team_1_ufgm =  sum(tracking['UFGM'][:x])
# 				team_1_ufga =  sum(tracking['UFGA'][:x])
# 				team_1_ufg_pct = calc_pct(getDouble(team_1_ufgm), getDouble(team_1_ufga))

# 				team_2_id = tracking['TEAM_ID'][x]
# 				team_2_abbrev = tracking['TEAM_ABBREVIATION'][x]
# 				team_2_dist = sum(tracking['DIST'][x:])
# 				team_2_touches = sum(tracking['TCHS'][x:])
# 				team_2_passes = sum(tracking['PASS'][x:])
# 				team_2_assist = sum(tracking['AST'][x:])
# 				team_2_sec_ast = sum(tracking['SAST'][x:])
# 				team_2_ft_ast = sum(tracking['FTAST'][x:])
# 				team_2_d_atrim_fgm = sum(tracking['DFGM'][x:])
# 				team_2_d_atrim_fga = sum(tracking['DFGA'][x:])
# 				team_2_d_atrim_fg_pct = calc_pct(getDouble(team_2_d_atrim_fgm), getDouble(team_2_d_atrim_fga))
# 				team_2_oreb_chance = sum(tracking['ORBC'][x:])
# 				team_2_dreb_chance = sum(tracking['DRBC'][x:])
# 				team_2_reb_chance = sum(tracking['RBC'][x:])
# 				team_2_cfgm =  sum(tracking['CFGM'][x:])
# 				team_2_cfga =  sum(tracking['CFGA'][x:])
# 				team_2_cfg_pct = calc_pct(getDouble(team_2_cfgm), getDouble(team_2_cfga))
# 				team_2_ufgm =  sum(tracking['UFGM'][x:])
# 				team_2_ufga =  sum(tracking['UFGA'][x:])
# 				team_2_ufg_pct = calc_pct(getDouble(team_2_ufgm), getDouble(team_2_ufga))

# 				create_team_table_tracking(team_1_abbrev)
# 				create_team_table_tracking(team_2_abbrev)

# 				team_table_arr = ["season", "game_id", "team_id", "team_abbrev", "dist_trav", "touches", "passes", "assist", "sec_ast", "ft_ast", "d_atrim_fgm", "d_atrim_fga", "d_atrim_fg_pct", "oreb_chance", "dreb_chance", "reb_chance", "cfgm", "cfga", "cfg_pct", "ufgm", "ufga", "ufga_pct"]
# 				team_1_value_arr = [season, game_id, team_1_id, team_1_abbrev, team_1_dist, team_1_touches, team_1_passes, team_1_assist, team_1_sec_ast, team_1_ft_ast, team_1_d_atrim_fgm, team_1_d_atrim_fga, team_1_d_atrim_fg_pct, team_1_oreb_chance, team_1_dreb_chance, team_1_reb_chance, team_1_cfgm, team_1_cfga, team_1_cfg_pct, team_1_ufgm, team_1_ufga, team_1_ufg_pct]
# 				team_2_value_arr = [season, game_id, team_2_id, team_2_abbrev, team_2_dist, team_2_touches, team_2_passes, team_2_assist, team_2_sec_ast, team_2_ft_ast, team_2_d_atrim_fgm, team_2_d_atrim_fga, team_2_d_atrim_fg_pct, team_2_oreb_chance, team_2_dreb_chance, team_2_reb_chance, team_2_cfgm, team_2_cfga, team_2_cfg_pct, team_2_ufgm, team_2_ufga, team_2_ufg_pct]

# 				insert_entry("teams_tracking", team_1_abbrev, team_table_arr, team_1_value_arr)
# 				insert_entry("teams_tracking", team_2_abbrev, team_table_arr, team_2_value_arr)
		
		
# 		create_player_table_tracking(table_ref)
# 		player_table_arr = ["season", "game_id", "player_id", "player_name", "minutes", "dist_trav", "avg_speed", "touches", "passes", "assist", "sec_ast", "ft_ast", "d_atrim_fgm", "d_atrim_fga", "d_atrim_fg_pct", "oreb_chance", "dreb_chance", "reb_chance", "fg_pct", "cfgm", "cfga", "cfg_pct", "ufgm", "ufga", "ufga_pct"]
# 		player_value_arr = [season, game_id, player_id, player_name, minutes, dist_trav, avg_speed, touches, passes, assists, sec_ast, ft_ast, d_atrim_fgm, d_atrim_fga, d_atrim_fg_pct, oreb_chance, dreb_chance, reb_chance, fg_pct, cfgm, cfga, cfg_pct, ufgm, ufga, ufg_pct]
# 		insert_entry("players_tracking", table_ref, player_table_arr, player_value_arr)


# def parse_usage(game_id, season):
# 	box = game.BoxscoreUsage(game_id)
# 	usage_stats = box.sql_players_usage()

# 	for x in range(0, usage_stats['PLAYER_NAME'].count()):
# 		player_id = usage_stats['PLAYER_ID'][x]
# 		player_name = usage_stats['PLAYER_NAME'][x].replace("'", "")
# 		player_name = player_name.replace("-", "")
# 		player_name = player_name.replace(".", "")

# 		table_ref = get_table_ref(player_name)
# 		team_id = usage_stats['TEAM_ID'][x]
# 		minutes = convertMinutes(usage_stats['MIN'][x])
# 		usg_pct = getDouble(usage_stats['USG_PCT'][x])
# 		pct_fgm = getDouble(usage_stats['PCT_FGM'][x])
# 		pct_fga = getDouble(usage_stats['PCT_FGA'][x])
# 		pct_3pm = getDouble(usage_stats['PCT_FG3M'][x])
# 		pct_3pa = getDouble(usage_stats['PCT_FG3A'][x])
# 		pct_ftm = getDouble(usage_stats['PCT_FTM'][x])
# 		pct_fta = getDouble(usage_stats['PCT_FTA'][x])
# 		pct_oreb = getDouble(usage_stats['PCT_OREB'][x])
# 		pct_dreb = getDouble(usage_stats['PCT_DREB'][x])
# 		pct_reb = getDouble(usage_stats['PCT_REB'][x])
# 		pct_ast = getDouble(usage_stats['PCT_AST'][x])
# 		pct_tov = getDouble(usage_stats['PCT_TOV'][x])
# 		pct_stl = getDouble(usage_stats['PCT_STL'][x])
# 		pct_blk = getDouble(usage_stats['PCT_BLK'][x])
# 		pct_blka = getDouble(usage_stats['PCT_BLKA'][x])
# 		pct_pf = getDouble(usage_stats['PCT_PF'][x])
# 		pct_pfd = getDouble(usage_stats['PCT_PFD'][x])
# 		pct_pts = getDouble(usage_stats['PCT_PTS'][x])

# 		value_arr = [season, game_id, team_id, player_id, player_name, minutes, usg_pct, pct_fgm, pct_fga, pct_3pm, pct_3pa, pct_ftm, pct_fta, pct_oreb, pct_dreb, pct_reb, pct_ast, pct_tov, pct_stl, pct_blk, pct_blka, pct_pf, pct_pfd, pct_pts]
# 		table_arr = ["season", "game_id", "team_id", "player_id", "player_name", "minutes", "usg_pct", "pct_fgm", "pct_fga", "pct_3pm", "pct_3pa", "pct_ftm", "pct_fta", "pct_oreb", "pct_dreb", "pct_reb", "pct_ast", "pct_tov", "pct_stl", "pct_blk", "pct_blka", "pct_pf", "pct_pfd", "pct_pts"]

# 		create_player_table_usage(table_ref)
# 		insert_entry("players_usage", table_ref, table_arr, value_arr)


# def parse_scoring(game_id, season):
# 	box = game.BoxscoreScoring(game_id)
# 	player_scoring = box.sql_players_scoring() 
# 	team_scoring = box.sql_team_scoring()

# 	for x in range(0, player_scoring['PLAYER_NAME'].count()):		

# 		player_id = player_scoring['PLAYER_ID'][x]
# 		player_name = player_scoring['PLAYER_NAME'][x].replace("'", "")
# 		player_name = player_name.replace("-", "")
# 		player_name = player_name.replace(".", "")
# 		table_ref = get_table_ref(player_name)

# 		minutes = convertMinutes(player_scoring['MIN'][x])
# 		pct_pts_2pt = getDouble(player_scoring['PCT_PTS_2PT'][x]) 
# 		pct_pts_midrange = getDouble(player_scoring['PCT_PTS_2PT_MR'][x]) 
# 		pct_pts_fastbreak = getDouble(player_scoring['PCT_PTS_FB'][x])
# 		pts_pts_ft = getDouble(player_scoring['PCT_PTS_FT'][x])
# 		pct_pts_offto = getDouble(player_scoring['PCT_PTS_OFF_TOV'][x])
# 		pct_pts_paint = getDouble(player_scoring['PCT_PTS_PAINT'][x])
# 		pct_2ptfgm_ast = getDouble(player_scoring['PCT_AST_2PM'][x])
# 		pct_2ptfgm_uast = getDouble(player_scoring['PCT_UAST_2PM'][x])
# 		pct_3ptfgm_ast = getDouble(player_scoring['PCT_AST_3PM'][x])
# 		pct_3ptfgm_uast = getDouble(player_scoring['PCT_UAST_3PM'][x])
# 		pct_fgm_ast = getDouble(player_scoring['PCT_AST_FGM'][x])
# 		pct_fgm_uast = getDouble(player_scoring['PCT_UAST_FGM'][x])

# 		player_value_arr = [season, game_id, player_id, player_name, minutes, pct_pts_2pt, pct_pts_midrange, pct_pts_fastbreak, pts_pts_ft, pct_pts_offto, pct_pts_paint, pct_2ptfgm_ast, pct_2ptfgm_uast, pct_3ptfgm_ast, pct_3ptfgm_uast, pct_fgm_ast, pct_fgm_uast]
# 		player_table_arr = ["season", "game_id", "player_id", "player_name", "minutes", "pct_pts_2pt", "pct_pts_midrange", "pct_pts_fastbreak", "pts_pts_ft", "pct_pts_offto", "pct_pts_paint", "pct_2ptfgm_ast", "pct_2ptfgm_uast", "pct_3ptfgm_ast", "pct_3ptfgm_uast", "pct_fgm_ast", "pct_fgm_uast"]

# 		if x > 4:
# 			if (player_scoring['TEAM_ID'][x] != player_scoring['TEAM_ID'][x - 1]):

# 				team1_id = player_scoring['TEAM_ID'][x - 1]
# 				team1_abbrev = player_scoring['TEAM_ABBREVIATION'][x - 1]

# 				if (team1_id == team_scoring['TEAM_ID'][0]):
# 					index1 = 0
# 					index2 = 1
# 				else:
# 					index1 = 1
# 					index2 = 0

# 				team1_pct_fga_2pt = getDouble(team_scoring['PCT_FGA_2PT'][index1])
# 				team1_pct_fga_3pt = getDouble(team_scoring['PCT_FGA_3PT'][index1])
# 				team1_pct_pts_2pt = getDouble(team_scoring['PCT_PTS_2PT'][index1])
# 				team1_pct_pts_midrange = getDouble(team_scoring['PCT_PTS_2PT_MR'][index1]) 
# 				team1_pct_pts_fastbreak = getDouble(team_scoring['PCT_PTS_FB'][index1])
# 				team1_pts_pts_ft = getDouble(team_scoring['PCT_PTS_FT'][index1])
# 				team1_pct_pts_offto = getDouble(team_scoring['PCT_PTS_OFF_TOV'][index1])
# 				team1_pct_pts_paint = getDouble(team_scoring['PCT_PTS_PAINT'][index1])
# 				team1_pct_2ptfgm_ast = getDouble(team_scoring['PCT_AST_2PM'][index1])
# 				team1_pct_2ptfgm_uast = getDouble(team_scoring['PCT_UAST_2PM'][index1])
# 				team1_pct_3ptfgm_ast = getDouble(team_scoring['PCT_AST_3PM'][index1])
# 				team1_pct_3ptfgm_uast = getDouble(team_scoring['PCT_UAST_3PM'][index1])
# 				team1_pct_fgm_ast = getDouble(team_scoring['PCT_AST_FGM'][index1])
# 				team1_pct_fgm_uast = getDouble(team_scoring['PCT_UAST_FGM'][index1])

# 				team2_id = player_scoring['TEAM_ID'][x]
# 				team2_abbrev = player_scoring['TEAM_ABBREVIATION'][x]
# 				team2_pct_fga_2pt = getDouble(team_scoring['PCT_FGA_2PT'][index2])
# 				team2_pct_fga_3pt = getDouble(team_scoring['PCT_FGA_3PT'][index2])
# 				team2_pct_pts_2pt = getDouble(team_scoring['PCT_PTS_2PT'][index2]) 
# 				team2_pct_pts_midrange = getDouble(team_scoring['PCT_PTS_2PT_MR'][index2]) 
# 				team2_pct_pts_fastbreak = getDouble(team_scoring['PCT_PTS_FB'][index2])
# 				team2_pts_pts_ft = getDouble(team_scoring['PCT_PTS_FT'][index2])
# 				team2_pct_pts_offto = getDouble(team_scoring['PCT_PTS_OFF_TOV'][index2])
# 				team2_pct_pts_paint = getDouble(team_scoring['PCT_PTS_PAINT'][index2])
# 				team2_pct_2ptfgm_ast = getDouble(team_scoring['PCT_AST_2PM'][index2])
# 				team2_pct_2ptfgm_uast = getDouble(team_scoring['PCT_UAST_2PM'][index2])
# 				team2_pct_3ptfgm_ast = getDouble(team_scoring['PCT_AST_3PM'][index2])
# 				team2_pct_3ptfgm_uast = getDouble(team_scoring['PCT_UAST_3PM'][index2])
# 				team2_pct_fgm_ast = getDouble(team_scoring['PCT_AST_FGM'][index2])
# 				team2_pct_fgm_uast = getDouble(team_scoring['PCT_UAST_FGM'][index2])

# 				team1_value_arr = [season, game_id, team1_id, team1_abbrev, team1_pct_fga_2pt, team1_pct_fga_3pt, team1_pct_pts_2pt, team1_pct_pts_midrange, team1_pct_pts_fastbreak, team1_pts_pts_ft, team1_pct_pts_offto, team1_pct_pts_paint, team1_pct_2ptfgm_ast, team1_pct_2ptfgm_uast, team1_pct_3ptfgm_ast, team1_pct_3ptfgm_uast, team1_pct_fgm_ast, team1_pct_fgm_uast]
# 				team2_value_arr = [season, game_id, team2_id, team2_abbrev, team2_pct_fga_2pt, team2_pct_fga_3pt, team2_pct_pts_2pt, team2_pct_pts_midrange, team2_pct_pts_fastbreak, team2_pts_pts_ft, team2_pct_pts_offto, team2_pct_pts_paint, team2_pct_2ptfgm_ast, team2_pct_2ptfgm_uast, team2_pct_3ptfgm_ast, team2_pct_3ptfgm_uast, team2_pct_fgm_ast, team2_pct_fgm_uast]				
# 				team_table_arr = ["season", "game_id", "team_id", "team_abbrev", "pct_fga_2pt", "pct_fga_3pt", "pct_pts_2pt", "pct_pts_midrange", "pct_pts_fastbreak", "pts_pts_ft", "pct_pts_offto", "pct_pts_paint", "pct_2ptfgm_ast", "pct_2ptfgm_uast", "pct_3ptfgm_ast", "pct_3ptfgm_uast", "pct_fgm_ast", "pct_fgm_uast"]

# 				create_team_table_scoring(team1_abbrev)
# 				create_team_table_scoring(team2_abbrev)
# 				insert_entry("teams_scoring", team1_abbrev, team_table_arr, team1_value_arr)
# 				insert_entry("teams_scoring", team2_abbrev, team_table_arr, team2_value_arr)

		
# 		create_player_table_scoring(table_ref)
# 		insert_entry("players_scoring", table_ref, player_table_arr, player_value_arr)


