# #Entry point of Hoops database loading scripts. How game IDs work:
# #Regular season game id format: 0021300001 = first game of 2013-2014 season. 1230 games per season
# #2013 playoffs id: 0041300101 - change 2 to 4 for playoffs then follow:
# #101: 1 = round, 0 = matchup (so first round goes 0-7, second 0-3, etc), 1 = game in series
# from iterator import *
# from nba_py import game
# from nba_py import team
# from util import *
# from coaches import *


# #bsa = game.BoxscoreAdvanced('0021400046')
# #print(bsa.team_stats())
# #info = game.BoxscoreSummary('0021400002')
# #game_summary = info.other_stats()
# #print(game_summary)
# #game_date = str(game_summary['GAME_DATE_EST'][0])[:10]
# #print(bsa.sql_team_advanced())
# #pt = game.PlayerTracking('0021600265')
# #print(pt.info())

# #box = game.Boxscore('0021600266')
# #player_stats = box.player_stats()
# #print(player_stats)

# #coaches = team.TeamCommonRoster('1610612739', '2015-16')
# #print(coaches.coaches())

# #coach_coon = find_coach('0021401230', 'SAC', '2015')
# #print(coach_coon)

# #box = game.BoxscoreUsage('0021600266')
# #print(box.sql_team_usage())


# season = 2015
# #betting relies on games_info,fantasy relies on betting

# #info = game.BoxscoreSummary('0021400008')
# #print("gamesummary")
# #print(info.game_summary())

# #insert_season(season,'0021500777')
# #insert_playoffs(season, '0041500')
# #insert_betting('betting/odds1516.csv', season)
# #insert_fantasy('fantasy/1516fantasy.txt', season)
