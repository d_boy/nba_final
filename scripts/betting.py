# from __future__ import print_function
# from nba_py import game
# import psycopg2
# import pandas
# from util import *
# from db_functions import *
# from fantasy import *

# def parse_betting(away_row, home_row, season):

# 	away_line_arr = away_row.split(',')
# 	game_date = away_line_arr[0]
# 	away_team_city = away_line_arr[3]
# 	away_team_id = get_team_id_from_city(away_team_city)
# 	away_team_abbrev = get_team_abbrev_from_id(away_team_id)
# 	away_team_score = getInt(away_line_arr[8])
# 	away_team_ml = getInt(away_line_arr[11])
# 	pickem = 0
# 	line_switch = None

# 	if (away_line_arr[9] == "pk"):
# 		line_open = 0
# 		if (getDouble(away_line_arr[10]) > 90):
# 			line_switch = True
# 	elif (getDouble(away_line_arr[9]) > 90):
# 		total_open = getDouble(away_line_arr[9])
# 		if (getDouble(away_line_arr[10]) < 90):
# 			line_switch = True
# 	else:
# 		line_open = getDouble(away_line_arr[9])

# 	if (away_line_arr[10] == "pk"):
# 		pickem = 1
# 		line_close = 0
# 		away_fav = 0
# 		away_dog = 0
# 	elif (getDouble(away_line_arr[10]) > 90):
# 		total_close = getDouble(away_line_arr[10])
# 		away_dog = 1
# 		away_fav = 0
# 	else:
# 		line_close = getDouble(away_line_arr[10])
# 		away_fav = 1
# 		away_dog = 0

# 	home_line_arr = home_row.split(',')
# 	home_team_city = home_line_arr[3]
# 	home_team_id = get_team_id_from_city(home_team_city)
# 	home_team_abbrev = get_team_abbrev_from_id(home_team_id)
# 	home_team_score = getInt(home_line_arr[8])
# 	home_team_ml = getInt(home_line_arr[11])

# 	if (home_line_arr[9] == "pk"):
# 		line_open = 0
# 		if (getDouble(home_line_arr[10]) > 90):
# 			line_switch = True
# 	elif (getDouble(home_line_arr[9]) > 90):
# 		total_open = getDouble(home_line_arr[9])
# 		if (getDouble(home_line_arr[10]) < 90):
# 			line_switch = True
# 	else:
# 		line_open = getDouble(home_line_arr[9])

# 	if (home_line_arr[10] == "pk"):
# 		pickem = 1
# 		line_close = 0
# 		home_fav = 0
# 		home_dog = 0
# 	elif (getDouble(home_line_arr[10]) > 90):
# 		total_close = getDouble(home_line_arr[10])
# 		home_dog = 1
# 		home_fav = 0
# 	else:
# 		line_close = getDouble(home_line_arr[10])
# 		home_fav = 1
# 		home_dog = 0

# 	if line_switch:
# 		line_movement = line_open + line_close
# 	else:
# 		line_movement = line_close - line_open

# 	total_movement = total_close - total_open
# 	actual_total = home_team_score + away_team_score

# 	game_id = assign_game_id_betting(game_date, season, away_team_id, home_team_id)
# 	print("uhhhhh {0}".format(game_id))

# 	away_team_days_rest = find_days_rest_team(away_team_abbrev, game_id)
# 	home_team_days_rest = find_days_rest_team(home_team_abbrev, game_id)
# 	last_meeting_id = get_last_meeting_id(game_id, season)
# 	away_team_btb = 0 if (away_team_days_rest > 0) else 1
# 	home_team_btb = 0 if (home_team_days_rest > 0) else 1

# 	if (away_team_score > home_team_score):
# 		away_wl = "W"
# 		home_wl = "L"
# 		margin = away_team_score - home_team_score
# 		if (away_dog == 1) or (pickem == 1):
# 			away_ats_result = "W"
# 			home_ats_result = "L"
# 		elif (margin > line_close):
# 			away_ats_result = "W"
# 			home_ats_result = "L"
# 		elif (margin < line_close):
# 			away_ats_result = "L"
# 			home_ats_result = "W"
# 	else:
# 		away_wl = "L"
# 		home_wl = "W"
# 		margin = home_team_score - away_team_score
# 		if (home_dog == 1) or (pickem == 1):
# 			home_ats_result = "W"
# 			away_ats_result = "L"
# 		elif (margin > line_close):
# 			home_ats_result = "W"
# 			away_ats_result = "L"
# 		elif (margin < line_close):
# 			home_ats_result = "L"
# 			away_ats_result = "W"

# 	if (margin == line_close):
# 		away_ats_result = "Push"
# 		home_ats_result = "Push"

# 	if (actual_total > total_close):
# 		over_under_result = "over"
# 	elif (actual_total < total_close):
# 		over_under_result = "under"
# 	else:
# 		over_under_result = "push"

# 	playoff_game = check_if_playoffs(season, game_id)

# 	table_arr = ["season", "game_id", "team_id", "opp_team_id", "playoff_game", "wl_outright", "team_abbrev", "opp_team_abbrev", "away_team_city", "home_team_city", "home_or_away", "fav", "dog", "pickem", "line_open", "line_close", "line_movement", "total_open", "total_close", "total_movement", "team_ml", "opp_ml", "team_actual_total", "opp_team_actual_total", "actual_total", "team_days_rest", "opp_team_days_rest", "team_btb", "opp_team_btb", "over_under_result", "ats_result", "last_meeting_id"]
# 	home_arr = [season, game_id, home_team_id, away_team_id, playoff_game, home_wl, home_team_abbrev, away_team_abbrev, away_team_city, home_team_city, "home", home_fav, home_dog, pickem, line_open, line_close, line_movement, total_open, total_close, total_movement, home_team_ml, away_team_ml, home_team_score, away_team_score, actual_total, home_team_days_rest, away_team_days_rest, home_team_btb, away_team_btb, over_under_result, home_ats_result, last_meeting_id]
# 	away_arr = [season, game_id, away_team_id, home_team_id, playoff_game, away_wl, away_team_abbrev, home_team_abbrev, away_team_city, home_team_city, "away", away_fav, away_dog, pickem, line_open, line_close, line_movement, total_open, total_close, total_movement, away_team_ml, home_team_ml, away_team_score, home_team_score, actual_total, away_team_days_rest, home_team_days_rest, away_team_btb, home_team_btb, over_under_result, away_ats_result, last_meeting_id]

# 	create_teams_betting_table(home_team_abbrev)
# 	create_teams_betting_table(away_team_abbrev)

# 	insert_entry("teams_betting", home_team_abbrev, table_arr, home_arr)
# 	insert_entry("teams_betting", away_team_abbrev, table_arr, away_arr)
# 	print("done! {0}".format(game_id))



# def assign_game_id_betting(game_date, season, away_team_id, home_team_id):
	
# 	if (len(game_date) == 3):
# 		year = getInt(season) + 1
# 		month = "0{0}".format(game_date[0])
# 		day = game_date[1:3]
# 	elif (len(game_date) == 4):
# 		year = getInt(season)
# 		month = game_date[0:2]
# 		day = game_date[2:4]

# 	new_date = "{0}-{1}-{2}".format(year, month, day)
# 	associated_id = find_game_id(new_date, season, away_team_id, home_team_id)

# 	return associated_id
