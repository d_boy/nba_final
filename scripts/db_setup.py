import psycopg2

conn = psycopg2.connect("dbname=Hoops2 user=nicholascaradonna password=cornhole")
cur = conn.cursor()

# cur.execute("DROP SCHEMA IF EXISTS players_basic")
# cur.execute("DROP SCHEMA IF EXISTS players_advanced")
# cur.execute("DROP SCHEMA IF EXISTS players_tracking")
# cur.execute("DROP SCHEMA IF EXISTS players_scoring")
# cur.execute("DROP SCHEMA IF EXISTS players_usage")
# cur.execute("DROP SCHEMA IF EXISTS teams_info")
# cur.execute("DROP SCHEMA IF EXISTS teams_basic")
# cur.execute("DROP SCHEMA IF EXISTS teams_advanced")
# cur.execute("DROP SCHEMA IF EXISTS teams_tracking")
# cur.execute("DROP SCHEMA IF EXISTS teams_scoring")

# cur.execute("DROP SCHEMA IF EXISTS teams_betting")
# cur.execute("DROP SCHEMA IF EXISTS players_fantasy")

# cur.execute("DROP SCHEMA IF EXISTS games_info")

# cur.execute("DROP SCHEMA IF EXISTS players")
# cur.execute("DROP SCHEMA IF EXISTS teams")
# cur.execute("DROP SCHEMA IF EXISTS officials")
# cur.execute("DROP SCHEMA IF EXISTS coaches")

# conn.commit()

cur.execute("CREATE SCHEMA IF NOT EXISTS players_basic")
cur.execute("CREATE SCHEMA IF NOT EXISTS players_advanced")
cur.execute("CREATE SCHEMA IF NOT EXISTS players_tracking")
cur.execute("CREATE SCHEMA IF NOT EXISTS players_scoring")
cur.execute("CREATE SCHEMA IF NOT EXISTS players_usage")
cur.execute("CREATE SCHEMA IF NOT EXISTS teams_info")
cur.execute("CREATE SCHEMA IF NOT EXISTS teams_basic")
cur.execute("CREATE SCHEMA IF NOT EXISTS teams_advanced")
cur.execute("CREATE SCHEMA IF NOT EXISTS teams_tracking")
cur.execute("CREATE SCHEMA IF NOT EXISTS teams_scoring")

cur.execute("CREATE SCHEMA IF NOT EXISTS teams_betting")
cur.execute("CREATE SCHEMA IF NOT EXISTS players_fantasy")

cur.execute("CREATE SCHEMA IF NOT EXISTS games_info")

cur.execute("CREATE SCHEMA IF NOT EXISTS players")
cur.execute("CREATE SCHEMA IF NOT EXISTS teams")
cur.execute("CREATE SCHEMA IF NOT EXISTS officials")
cur.execute("CREATE SCHEMA IF NOT EXISTS coaches")

conn.commit()

year = 2015

conn.commit()

cur.execute("CREATE TABLE IF NOT EXISTS games_info.season_{0} (id SERIAL, season integer, game_id text UNIQUE, game_date text, playoff_game integer, away_team_id text, home_team_id text, away_team_abbrev text, home_team_abbrev text, away_team_coach text, home_team_coach text, official_1_id text, official_2_id text, official_3_id text, official_4_id text, last_meeting_id text, winning_team_id text, losing_team_id text, winning_team_abbrev text, losing_team_abbrev text, away_team_score integer, home_team_score integer, winning_team_score integer, losing_team_score integer, overtime integer, attendance integer, away_team_pts_1q integer, away_team_pts_2q integer, away_team_pts_3q integer, away_team_pts_4q integer, away_team_pts_ot1 integer, away_team_pts_ot2 integer, away_team_pts_ot3 integer, away_team_pts_ot4 integer, home_team_pts_1q integer, home_team_pts_2q integer, home_team_pts_3q integer, home_team_pts_4q integer, home_team_pts_ot1 integer, home_team_pts_ot2 integer, home_team_pts_ot3 integer, home_team_pts_ot4 integer, total_score integer, PRIMARY KEY (game_id));".format(str(year)))
#decided these tables below were unnecessary, but leaving them commented because will need these values later
#cur.execute("CREATE TABLE games_basic.season_{0} (id SERIAL, season integer, game_id text UNIQUE, team_id text, team_abbrev text, away_team_id text, home_team_id text, away_team_abbrev text, home_team_abbrev text, player_id text, player_name text, start_position character, comment text, min decimal, fgm integer, fga integer, fg_pct decimal, threepm integer, threepa integer, threep_pct decimal, ftm integer, fta integer,ft_pct decimal, o_reb integer, d_reb integer, reb integer, assist integer, tov integer, stl integer, blk integer, pf integer, pts integer, plus_minus integer, FOREIGN KEY(game_id) REFERENCES games_info.season_{1}(game_id));".format(str(year), str(year)))
#cur.execute("CREATE TABLE games_advanced.season_{0} (id SERIAL, game_id text UNIQUE, player_id text, player_name text, min decimal, o_rtg decimal, d_rtg decimal, net_rtg decimal, ast_pct decimal, ast_to_ratio decimal, ast_ratio decimal, o_reb_pct decimal, d_reb_pct decimal, reb_pct decimal, to_ratio decimal, efg_pct decimal, ts_pct decimal, usage_pct decimal, pace decimal, pie decimal, FOREIGN KEY(game_id) REFERENCES games_info.season_{1}(game_id) );".format(str(year), str(year))) 
#cur.execute("CREATE TABLE games_tracking.season_{0} (id SERIAL, game_id text UNIQUE, player_id text, player_name text, min decimal, dist_trav decimal, avg_speed decimal, touches integer, passes integer, assist integer, sec_ast integer, ft_ast integer, d_atrim_fgm integer, d_atrim_fga integer, d_atrim_fg_pct integer, oreb_chance integer, dreb_chance integer, reb_chance integer, fg_pct decimal, cfgm integer, cfga integer, cfg_pct decimal, ufgm integer, ufga integer, ufga_pct decimal, FOREIGN KEY(game_id) REFERENCES games_info.season_{1}(game_id) );".format(str(year), str(year))) 
#cur.execute("CREATE TABLE games_scoring.season_{0} (id SERIAL, game_id text, player_id text, player_name text, min decimal, pct_fga_2pt decimal, pct_fga_3pt decimal, pct_pts_2pt decimal, pct_pts_2pt_mr decimal, pct_pts_3pt decimal, pct_pts_fbps decimal, pct_pts_ft decimal, pct_pts_off_to decimal, pct_pts_pitp decimal, pct_2ptfg_ast decimal, pct_2ptfg_unast decimal, pct_3ptfg_ast decimal, pct_3ptfg_unast decimal, pct_fgm_ast decimal, pct_fga_ast decimal, FOREIGN KEY(game_id) REFERENCES games_info.season_{1}(game_id) );".format(str(year), str(year)))
#cur.execute("CREATE TABLE games_usage.season_{0} (id SERIAL, game_id text, player_id text, player_name text, min decimal, usage_pct decimal, pct_team_fgm decimal, pct_team_fga decimal, pct_team_3pm decimal, pct_team_3pa decimal, pct_team_ftm decimal, pct_team_fta decimal, pct_team_oreb decimal, pct_team_dreb decimal, pct_team_reb decimal, pct_team_ast decimal, pct_team_to decimal, pct_team_stl decimal, pct_team_blk decimal, pct_team_blka decimal, pct_team_pf decimal, pct_team_pfd decimal, pct_team_pts decimal, FOREIGN KEY(game_id) REFERENCES games_info.season_{1}(game_id) );".format(str(year), str(year)))

cur.execute("CREATE TABLE IF NOT EXISTS players.players (id SERIAL, player_id text UNIQUE, first_name text, last_name text, table_ref text, PRIMARY KEY(player_id) );") 
cur.execute("CREATE TABLE IF NOT EXISTS teams.teams (id SERIAL, team_id text UNIQUE, team_abbrev text, team_city text, team_nickname text, PRIMARY KEY(team_id) );") 
cur.execute("CREATE TABLE IF NOT EXISTS officials.officials (id SERIAL, official_id text UNIQUE, first_name text, last_name text, table_ref text, PRIMARY KEY(official_id) );") 
cur.execute("CREATE TABLE IF NOT EXISTS coaches.coaches (id SERIAL, coach_name text UNIQUE)")

conn.commit()
print("done!")