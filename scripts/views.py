import requests
from django.http import HttpResponse
from scripts.projector import *
from django.http import JsonResponse
from django.shortcuts import render
from .forms import FilterForm

def index(request):
	#return HttpResponse(get_projector_data())
	#r = requests.get('http://httpbin.org/status/418')
	#print(r.text)
	return graph(request)

def graph(request):
	form = FilterForm(request.POST)	
	if (request.method == "POST"):
		print("post_parser")
		request.session['data'] = post_parser(request)
	else:
		print("set_params")
		request.session['data'] = set_params()

	return render(request, 'graph/graph.html', {'form': form})


def game_graph_data(request):
	data = {}
	# if request.session['data'] is None:
	# 	with open('scripts/graphData.json') as json_data:
	# 		data = json.load(json_data)
	# else:
	data = request.session['data']

	return JsonResponse(list(data), safe=False)
