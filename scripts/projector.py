# whats up

from scripts.db_functions import *
from scripts.team_data import *
from scripts.util import *
import math
# import numpy as np
# import pandas as pd
import json


# from views import game_graph_data

# team_test = team.TeamLastNGamesSplits('1610612737')
# print(team_test.last10().columns)
# print(team_test.last10().values)
# glob_additional = None
# glob_last = None
# glob_last_tracking = None
# glob_x_axis = 'spread_difference'
# glob_y_axis = 'total_difference'

def set_params(additional=None, last_game=None, last_game_tracking=None,
               x_axis={'tech': "spread_difference", 'display': "Spread Difference"},
               y_axis={'tech': "total_difference", 'display': "Total Difference"}):
    # def set_params():
    # List of additional params that can be queried for.
    # Params set on/off by flag, then specified. (ex: nothing will happen if fav_or_dog is 0, and fav = 1.
    # If unwanted, pass None
    # Betting params:
    additional_params = {}

    if (additional is None):
        additional_params['on'] = 0
        additional_params['team'] = "All"
        additional_params['season'] = []
        additional_params['playoffs_only'] = 0
        additional_params['reg_season_only'] = 0
        additional_params['days_rest'] = 0
        additional_params['days_rest_min'] = 0
        additional_params['days_rest_max'] = 0
        additional_params['opp_days_rest'] = 0
        additional_params['opp_days_rest_min'] = 0
        additional_params['opp_days_rest_max'] = 0
        additional_params['fav_or_dog'] = 0
        additional_params['fav'] = 1
        additional_params['dog'] = 0
        additional_params['pickem'] = 0
        additional_params['home_or_away'] = 0
        additional_params['home'] = 1
        additional_params['away'] = 0
        additional_params['opp_team'] = None
        additional_params['vs_conference'] = 0
        additional_params['vs_east'] = 1
        additional_params['vs_west'] = 0
        additional_params['is_conference'] = 0
        additional_params['east'] = 1
        additional_params['west'] = 0
        additional_params['line_close'] = 0
        additional_params['line_close_min'] = 0
        additional_params['line_close_max'] = 0
        additional_params['line_open'] = 0
        additional_params['line_open_min'] = 3
        additional_params['line_open_max'] = 10
        additional_params['line_movement'] = 0
        additional_params['line_movement_min'] = -3
        additional_params['line_movement_max'] = 0
        additional_params['total_close'] = 0
        additional_params['total_close_min'] = 0
        additional_params['total_close_max'] = 0
        additional_params['total_open'] = 0
        additional_params['total_open_min'] = 0
        additional_params['total_open_max'] = 0
        additional_params['total_movement'] = 0
        additional_params['total_movement_min'] = 0
        additional_params['total_movement_max'] = 0
        additional_params['wl_outright'] = 0
        additional_params['wl_outright_result'] = "W"
        additional_params['ats'] = 0
        additional_params['ats_result'] = "W"
        additional_params['over_under'] = 0
        additional_params['over_under_result'] = "over"
        additional_params['team_record'] = 0
        additional_params['team_record_min_win_pct'] = 0
        additional_params['team_record_max_win_pct'] = 1
        additional_params['opp_record'] = 0
        additional_params['opp_record_min_win_pct'] = 0
        additional_params['opp_record_max_win_pct'] = 1

        additional_params['coach_filter'] = "All"
        additional_params['official_filter'] = "All"
    else:
        additional_params = additional

    # List of last game conditions that can be queried for.
    # Params set on/off by flag, then specified (ex: nothing will happen if fav_or_dog is 0, and fav = 1.
    # If unwanted, set "on" attribute to 0
    last_game_params = {}

    if (last_game is None):
        last_game_params['on'] = 0
        last_game_params['home_or_away'] = 1
        last_game_params['home'] = 0
        last_game_params['away'] = 1
        last_game_params['opp_team'] = None
        last_game_params['vs_conference'] = 0
        last_game_params['east'] = 1
        last_game_params['west'] = 0
        last_game_params['fav_or_dog'] = 0
        last_game_params['fav'] = 1
        last_game_params['dog'] = 0
        last_game_params['pickem'] = 0
        last_game_params['wl_outright'] = 0
        last_game_params['wl_outright_result'] = "W"
        last_game_params['ats'] = 0
        last_game_params['ats_result'] = "W"
        last_game_params['over_under'] = 0
        last_game_params['over_under_result'] = "over"
        last_game_params['team_actual_total'] = 0
        last_game_params['team_actual_total_min'] = 100
        last_game_params['team_actual_total_max'] = 200
        last_game_params['opp_team_actual_total'] = 0
        last_game_params['opp_team_actual_total_min'] = 100
        last_game_params['opp_team_actual_total_max'] = 200
        last_game_params['opp_record'] = 0
        last_game_params['opp_record_min_win_pct'] = 0
        last_game_params['opp_record_max_win_pct'] = 1
        last_game_params['margin_of_victory'] = 0
        last_game_params['margin_of_victory_min'] = 0
        last_game_params['margin_of_victory_max'] = 0
        last_game_params['spread_difference'] = 0
        last_game_params['spread_difference_min'] = 0
        last_game_params['spread_difference_max'] = 0
        last_game_params['total_difference'] = 0
        last_game_params['total_difference_min'] = 0
        last_game_params['total_difference_max'] = 0

    else:
        last_game_params = last_game

    # Tracking params:
    # Last_game_params must be turned on as well for this to apply
    if (last_game_tracking is None):
        last_game_tracking_params = {}
        last_game_tracking_params['on'] = 0
        last_game_tracking_params['dist_trav'] = 1
        last_game_tracking_params['dist_trav_min'] = 17
        last_game_tracking_params['dist_trav_max'] = 100
        last_game_tracking_params['cfg_pct'] = 1
        last_game_tracking_params['cfg_pct_min'] = 60
        last_game_tracking_params['cfg_pct_max'] = 100
    else:
        last_game_tracking_params = last_game_tracking

    if (additional_params['team'] == "All"):
        print("allteamslast")
        all_last = AllTeamsLastNGamesBetting(additional_params, last_game_params, last_game_tracking_params)
    else:
        print("teamlastN")
        all_last = TeamLastNGamesBetting(additional_params['team'], 100, None, additional_params, last_game_params,
                                         last_game_tracking_params)

    last_n = all_last.last_N()

    if last_n is None:
        print("no data")
        no_data = {'x_axis': x_axis['display'], 'y_axis': y_axis['display']}
        # with open('scripts/graphData.json', 'w') as outfile:
        #     json.dump(no_data, outfile)
        return no_data

    x_vals = last_n[x_axis['tech']]
    y_vals = last_n[y_axis['tech']]
    home_or_aways = last_n['home_or_away']
    team_abbrevs = last_n['team_abbrev']
    opp_team_abbrevs = last_n['opp_team_abbrev']
    game_ids = last_n['game_id']
    at_or_vses = []

    for x in range(0, len(x_vals)):
        if (home_or_aways[x] == "home"):
            at_or_vses.append("vs.")
        elif (home_or_aways[x] == "away"):
            at_or_vses.append("@")

    game_dates = []

    for p in range(0, len(x_vals)):
        game_date = find_game_date(game_ids[p], team_abbrevs[p])
        game_dates.append(game_date)

    descriptions = []
    for q in range(0, len(x_vals)):
        descrip = "{0} {1} {2}, {3}".format(team_abbrevs[q], at_or_vses[q], opp_team_abbrevs[q], game_dates[q])
        descriptions.append(descrip)

    spreads = last_n['line_close']
    totals = last_n['total_close']
    favs = last_n['fav']
    dogs = last_n['dog']
    spread_texts = []

    for y in range(0, len(x_vals)):
        if (favs[y] == 1):
            spread = "-{0}".format(spreads[y])
        elif (dogs[y] == 1):
            spread = "+{0}".format(spreads[y])
        else:
            spread = "PK"

        spread_texts.append(spread)

    projections = []
    for z in range(0, len(x_vals)):
        projected = "Spread: {0} Total: {1}".format(spread_texts[z], totals[z])
        projections.append(projected)

    team_actual_totals = last_n['team_actual_total']
    opp_team_actual_totals = last_n['opp_team_actual_total']
    actuals = []
    for o in range(0, len(x_vals)):
        actual = "Result: {0} {1} {2} {3}".format(team_abbrevs[o], getInt(team_actual_totals[o]), opp_team_abbrevs[o],
                                                  getInt(opp_team_actual_totals[o]))
        actuals.append(actual)

    hyperlinks = []
    for box_score_id in game_ids:
        hyperlink = "http://stats.nba.com/game/#!/{0}/".format(box_score_id)
        hyperlinks.append(hyperlink)

    result = []
    used_game_ids = []
    for num in range(0, len(x_vals)):
        data_values = {'x_value': x_vals[num], 'y_value': y_vals[num], 'description': descriptions[num],
                       'projection': projections[num], 'actual': actuals[num], 'hyperlink': hyperlinks[num]}
        if game_ids[num] not in used_game_ids:
            result.append(data_values)
            used_game_ids.append(game_ids[num])

    result.append({'x_axis': x_axis['display'], 'y_axis': y_axis['display']})

    # print(rsquared(x_vals, y_vals))
    if len(x_vals) > 0:
        x_avg = '%.3f' % (find_mean(x_vals))
        y_avg = '%.3f' % (find_mean(y_vals))
        rsq = '%.3f' % (rsquared(x_vals, y_vals))
        # rsq = 0
        result.append({'x_avg': x_avg, 'y_avg': y_avg, 'rsquared': rsq})
    # print to json file
    # with open('scripts/graphData.json', 'w') as outfile:
    #     json.dump(result, outfile)

    return result


# print(last_n['actual_total'])
# print(find_mean(y_vals))
# print(np.corrcoef(x_vals, y_vals)[0, 1])
# print(rsquared(x_vals, y_vals))


def post_parser(data):
    x_axis = {}

    if (data.POST.get('u_filter_x_axis') == "total_diff"):
        x_axis = {'tech': "total_difference", 'display': "Total Difference"}
    elif (data.POST.get('u_filter_x_axis') == "spread_diff"):
        x_axis = {'tech': "spread_difference", 'display': "Spread Difference"}
    elif (data.POST.get('u_filter_x_axis') == "margin_of_victory"):
        x_axis = {'tech': "margin_of_victory", 'display': "Margin of Victory"}
    elif (data.POST.get('u_filter_x_axis') == "total"):
        x_axis = {'tech': "actual_total", 'display': "Game Total"}
    elif (data.POST.get('u_filter_x_axis') == "points_scored"):
        x_axis = {'tech': "team_actual_total", 'display': "Points Scored"}
    elif (data.POST.get('u_filter_x_axis') == "points_allowed"):
        x_axis = {'tech': "opp_team_actual_total", 'display': "Points Allowed"}
    # elif (data.POST.get('u_filter_x_axis') == "o_rtg"):
    # 	x_axis = {'tech': "off_rating", 'display': "Offensive Rating"}
    # elif (data.POST.get('u_filter_x_axis') == "d_rtg"):
    # 	x_axis = {'tech': "def_rating", 'display': "Defensive Rating"}
    # elif (data.POST.get('u_filter_x_axis') == "net_rtg"):
    # 	x_axis = {'tech': "net_rating", 'display': "Net Rating"}

    y_axis = ""

    if (data.POST.get('u_filter_y_axis') == "total_diff"):
        y_axis = {'tech': "total_difference", 'display': "Total Difference"}
    elif (data.POST.get('u_filter_y_axis') == "spread_diff"):
        y_axis = {'tech': "spread_difference", 'display': "Spread Difference"}
    elif (data.POST.get('u_filter_y_axis') == "margin_of_victory"):
        y_axis = {'tech': "margin_of_victory", 'display': "Margin of Victory"}
    elif (data.POST.get('u_filter_y_axis') == "total"):
        y_axis = {'tech': "actual_total", 'display': "Game Total"}
    elif (data.POST.get('u_filter_y_axis') == "points_scored"):
        y_axis = {'tech': "team_actual_total", 'display': "Points Scored"}
    elif (data.POST.get('u_filter_y_axis') == "points_allowed"):
        y_axis = {'tech': "opp_team_actual_total", 'display': "Points Allowed"}
    # elif (data.POST.get('u_filter_y_axis') == "o_rtg"):
    # 	y_axis = {'tech': "off_rating", 'display': "Offensive Rating"}
    # elif (data.POST.get('u_filter_y_axis') == "d_rtg"):
    # 	y_axis = {'tech': "def_rating", 'display': "Defensive Rating"}
    # elif (data.POST.get('u_filter_y_axis') == "net_rtg"):
    # 	y_axis = {'tech': "net_rating", 'display': "Net Rating"}

    additional_params = {}
    additional_params['on'] = 1

    if (data.POST.get('g_filter_teams') == "all_teams"):
        additional_params['team'] = "All"
        additional_params['is_conference'] = 0
        additional_params['east'] = 0
        additional_params['west'] = 0
    elif (data.POST.get('g_filter_teams') == "east_teams"):
        additional_params['team'] = "All"
        additional_params['is_conference'] = 1
        additional_params['east'] = 1
        additional_params['west'] = 0
    elif (data.POST.get('g_filter_teams') == "west_teams"):
        additional_params['team'] = "All"
        additional_params['is_conference'] = 1
        additional_params['east'] = 0
        additional_params['west'] = 1
    else:
        additional_params['team'] = data.POST.get('g_filter_teams')
        additional_params['is_conference'] = 0
        additional_params['east'] = 1
        additional_params['west'] = 0

    if (data.POST.get('g_filter_seasons') == "all_seasons"):
        additional_params['season'] = []
    else:
        additional_params['season'] = [data.POST.get('g_filter_seasons')]

    if (data.POST.get('g_filter_game_types') == "reg_and_playoffs"):
        additional_params['playoffs_only'] = 0
        additional_params['reg_season_only'] = 0
    elif (data.POST.get('g_filter_game_types') == "reg_season"):
        additional_params['playoffs_only'] = 0
        additional_params['reg_season_only'] = 1
    elif (data.POST.get('g_filter_game_types') == "playoffs_only"):
        additional_params['playoffs_only'] = 1
        additional_params['reg_season_only'] = 0

    if (data.POST.get('g_filter_days_rest') == "on"):
        additional_params['days_rest'] = 1
        if (data.POST.get('g_filter_days_rest_min') == ""):
            additional_params['days_rest_min'] = 0
        else:
            additional_params['days_rest_min'] = data.POST.get('g_filter_days_rest_min')

        if (data.POST.get('g_filter_days_rest_max') == ""):
            additional_params['days_rest_max'] = 0
        else:
            additional_params['days_rest_max'] = data.POST.get('g_filter_days_rest_max')
    else:
        additional_params['days_rest'] = 0
        additional_params['days_rest_min'] = 0
        additional_params['days_rest_max'] = 0

    if (data.POST.get('g_filter_opp_days_rest') == "on"):
        additional_params['opp_days_rest'] = 1
        if (data.POST.get('g_filter_opp_days_rest_min') == ""):
            additional_params['opp_days_rest_min'] = 0
        else:
            additional_params['opp_days_rest_min'] = data.POST.get('g_filter_opp_days_rest_min')

        if (data.POST.get('g_filter_opp_days_rest_max') == ""):
            additional_params['opp_days_rest_max'] = 0
        else:
            additional_params['opp_days_rest_max'] = data.POST.get('g_filter_opp_days_rest_max')
    else:
        additional_params['opp_days_rest'] = 0
        additional_params['opp_days_rest_min'] = 0
        additional_params['opp_days_rest_max'] = 0

    if (data.POST.get('g_filter_fav_or_dog') == "all_fav_or_dog"):
        additional_params['fav_or_dog'] = 0
        additional_params['fav'] = 0
        additional_params['dog'] = 0
        additional_params['pickem'] = 0
    elif (data.POST.get('g_filter_fav_or_dog') == "fav_only"):
        additional_params['fav_or_dog'] = 1
        additional_params['fav'] = 1
        additional_params['dog'] = 0
        additional_params['pickem'] = 0
    elif (data.POST.get('g_filter_fav_or_dog') == "dog_only"):
        additional_params['fav_or_dog'] = 1
        additional_params['fav'] = 0
        additional_params['dog'] = 1
        additional_params['pickem'] = 0
    else:
        additional_params['fav_or_dog'] = 1
        additional_params['fav'] = 0
        additional_params['dog'] = 0
        additional_params['pickem'] = 1

    if (data.POST.get('g_filter_home_or_away') == "both_home_or_away"):
        additional_params['home_or_away'] = 0
        additional_params['home'] = 0
        additional_params['away'] = 0
    elif (data.POST.get('g_filter_home_or_away') == "home_only"):
        additional_params['home_or_away'] = 1
        additional_params['home'] = 1
        additional_params['away'] = 0
    else:
        additional_params['home_or_away'] = 1
        additional_params['home'] = 0
        additional_params['away'] = 1

    # if (data.POST.get('g_filter_vs_conference') == "both_conferences"):
    #     additional_params['vs_conference'] = 0
    #     additional_params['vs_east'] = 0
    #     additional_params['vs_west'] = 0
    # elif (data.POST.get('g_filter_vs_conference') == "east_only"):
    #     additional_params['vs_conference'] = 1
    #     additional_params['vs_east'] = 1
    #     additional_params['vs_west'] = 0
    # else:
    #     additional_params['vs_conference'] = 1
    #     additional_params['vs_east'] = 0
    #     additional_params['vs_west'] = 1

    if (data.POST.get('g_filter_opp_teams') == "all_teams"):
        additional_params['opp_team'] = None
        additional_params['vs_conference'] = 0
        additional_params['vs_east'] = 0
        additional_params['vs_west'] = 0
    elif (data.POST.get('g_filter_opp_teams') == "east_teams"):
        additional_params['opp_team'] = None
        additional_params['vs_conference'] = 1
        additional_params['vs_east'] = 1
        additional_params['vs_west'] = 0
    elif (data.POST.get('g_filter_opp_teams') == "west_teams"):
        additional_params['opp_team'] = None
        additional_params['vs_conference'] = 1
        additional_params['vs_east'] = 0
        additional_params['vs_west'] = 1
    else:
        additional_params['opp_team'] = data.POST.get('g_filter_opp_teams')
        additional_params['vs_conference'] = 0
        additional_params['vs_east'] = 0
        additional_params['vs_west'] = 0

    if (data.POST.get('g_filter_spread') == "on"):
        additional_params['line_close'] = 1
        if (data.POST.get('g_filter_spread_min') == ""):
            additional_params['line_close_min'] = 0
        else:
            additional_params['line_close_min'] = data.POST.get('g_filter_spread_min')

        if (data.POST.get('g_filter_spread_max') == ""):
            additional_params['line_close_max'] = 1000
        else:
            additional_params['line_close_max'] = data.POST.get('g_filter_spread_max')
    else:
        additional_params['line_close'] = 0
        additional_params['line_close_min'] = 0
        additional_params['line_close_max'] = 0

    additional_params['line_open'] = 0
    additional_params['line_open_min'] = 0
    additional_params['line_open_max'] = 0
    additional_params['line_movement'] = 0
    additional_params['line_movement_min'] = 0
    additional_params['line_movement_max'] = 0

    if (data.POST.get('g_filter_total') == "on"):
        additional_params['total_close'] = 1
        if (data.POST.get('g_filter_total_min') == ""):
            additional_params['total_close_min'] = 0
        else:
            additional_params['total_close_min'] = data.POST.get('g_filter_total_min')

        if (data.POST.get('g_filter_total_max') == ""):
            additional_params['total_close_max'] = 1000
        else:
            additional_params['total_close_max'] = data.POST.get('g_filter_total_max')
    else:
        additional_params['total_close'] = 0
        additional_params['total_close_min'] = 0
        additional_params['total_close_max'] = 0

    additional_params['total_open'] = 0
    additional_params['total_open_min'] = 0
    additional_params['total_open_max'] = 0
    additional_params['total_movement'] = 0
    additional_params['total_movement_min'] = 0
    additional_params['total_movement_max'] = 0

    if (data.POST.get('g_filter_won_or_lost') == "both_won_or_lost"):
        additional_params['wl_outright'] = 0
        additional_params['wl_outright_result'] = "W"
    elif (data.POST.get('g_filter_won_or_lost') == "lost_only"):
        additional_params['wl_outright'] = 1
        additional_params['wl_outright_result'] = "L"
    else:
        additional_params['wl_outright'] = 1
        additional_params['wl_outright_result'] = "W"

    if (data.POST.get('g_filter_won_or_lost_ats') == "both_won_or_lost"):
        additional_params['ats'] = 0
        additional_params['wl_outright_result'] = "W"
    elif (data.POST.get('g_filter_won_or_lost_ats') == "lost_only"):
        additional_params['ats'] = 1
        additional_params['ats_result'] = "L"
    else:
        additional_params['ats'] = 1
        additional_params['ats_result'] = "W"

    if (data.POST.get('g_filter_over_or_under') == "both_over_or_under"):
        additional_params['over_under'] = 0
        additional_params['over_under_result'] = "over"
    elif (data.POST.get('g_filter_over_or_under') == "under_only"):
        additional_params['over_under'] = 1
        additional_params['over_under_result'] = "under"
    else:
        additional_params['over_under'] = 1
        additional_params['over_under_result'] = "over"

    if (data.POST.get('g_filter_record') == "on"):
        additional_params['team_record'] = 1
        if (data.POST.get('g_filter_win_pct_min') == ""):
            additional_params['team_record_min_win_pct'] = 0
        else:
            additional_params['team_record_min_win_pct'] = data.POST.get('g_filter_win_pct_min')

        if (data.POST.get('g_filter_win_pct_max') == ""):
            additional_params['team_record_max_win_pct'] = 1
        else:
            additional_params['team_record_max_win_pct'] = data.POST.get('g_filter_win_pct_max')
    else:
        additional_params['team_record'] = 0
        additional_params['team_record_min_win_pct'] = 0
        additional_params['team_record_max_win_pct'] = 0

    if (data.POST.get('g_filter_opponent_record') == "on"):
        additional_params['opp_record'] = 1
        if (data.POST.get('g_filter_opponent_win_pct_min') == ""):
            additional_params['opp_record_min_win_pct'] = 0
        else:
            additional_params['opp_record_min_win_pct'] = data.POST.get('g_filter_opponent_win_pct_min')

        if (data.POST.get('g_filter_opponent_win_pct_max') == ""):
            additional_params['opp_record_max_win_pct'] = 1
        else:
            additional_params['opp_record_max_win_pct'] = data.POST.get('g_filter_opponent_win_pct_max')
    else:
        additional_params['opp_record'] = 0
        additional_params['opp_record_min_win_pct'] = 0
        additional_params['opp_record_max_win_pct'] = 0

    additional_params['coach_filter'] = data.POST.get('g_filter_coach')
    additional_params['official_filter'] = data.POST.get('g_filter_official')

    last_game_params = {}
    last_game_params['on'] = 0

    if (data.POST.get('filter_last_game_home_or_away') == "both_home_or_away"):
        last_game_params['home_or_away'] = 0
        last_game_params['home'] = 0
        last_game_params['away'] = 0
    elif (data.POST.get('filter_last_game_home_or_away') == "home_only"):
        last_game_params['home_or_away'] = 1
        last_game_params['home'] = 1
        last_game_params['away'] = 0
    else:
        last_game_params['home_or_away'] = 1
        last_game_params['home'] = 0
        last_game_params['away'] = 1


    if (data.POST.get('filter_last_game_opp_team') == "all_teams"):
        last_game_params['opp_team'] = None
        last_game_params['opp_team_ind'] = 0
        last_game_params['vs_conference'] = 0
        last_game_params['vs_east'] = 0
        last_game_params['vs_west'] = 0
    elif (data.POST.get('filter_last_game_opp_team') == "east_teams"):
        last_game_params['opp_team'] = None
        last_game_params['opp_team_ind'] = 0
        last_game_params['vs_conference'] = 1
        last_game_params['vs_east'] = 1
        last_game_params['vs_west'] = 0
    elif (data.POST.get('filter_last_game_opp_team') == "west_teams"):
        last_game_params['opp_team'] = None
        last_game_params['opp_team_ind'] = 0
        last_game_params['vs_conference'] = 1
        last_game_params['vs_east'] = 0
        last_game_params['vs_west'] = 1
    else:
        last_game_params['opp_team'] = data.POST.get('filter_last_game_opp_team')
        last_game_params['opp_team_ind'] = 1
        last_game_params['vs_conference'] = 0
        last_game_params['vs_east'] = 0
        last_game_params['vs_west'] = 0

    if (data.POST.get('filter_last_game_fav_or_dog') == "all_fav_or_dog"):
        last_game_params['fav_or_dog'] = 0
        last_game_params['fav'] = 0
        last_game_params['dog'] = 0
        last_game_params['pickem'] = 0
    elif (data.POST.get('filter_last_game_fav_or_dog') == "fav_only"):
        last_game_params['fav_or_dog'] = 1
        last_game_params['fav'] = 1
        last_game_params['dog'] = 0
        last_game_params['pickem'] = 0
    elif (data.POST.get('filter_last_game_fav_or_dog') == "dog_only"):
        last_game_params['fav_or_dog'] = 1
        last_game_params['fav'] = 0
        last_game_params['dog'] = 1
        last_game_params['pickem'] = 0
    else:
        last_game_params['fav_or_dog'] = 1
        last_game_params['fav'] = 0
        last_game_params['dog'] = 0
        last_game_params['pickem'] = 1

    if (data.POST.get('filter_last_game_won_or_lost') == "both_won_or_lost"):
        last_game_params['wl_outright'] = 0
        last_game_params['wl_outright_result'] = "W"
    elif (data.POST.get('filter_last_game_won_or_lost') == "lost_only"):
        last_game_params['wl_outright'] = 1
        last_game_params['wl_outright_result'] = "L"
    else:
        last_game_params['wl_outright'] = 1
        last_game_params['wl_outright_result'] = "W"

    if (data.POST.get('filter_last_game_won_or_lost_ats') == "both_won_or_lost"):
        last_game_params['ats'] = 0
        last_game_params['ats_result'] = "W"
    elif (data.POST.get('filter_last_game_won_or_lost_ats') == "lost_only"):
        last_game_params['ats'] = 1
        last_game_params['ats_result'] = "L"
    else:
        last_game_params['ats'] = 1
        last_game_params['ats_result'] = "W"

    if (data.POST.get('filter_last_game_over_or_under') == "both_over_or_under"):
        last_game_params['over_under'] = 0
        last_game_params['over_under_result'] = "over"
    elif (data.POST.get('filter_last_game_over_or_under') == "under_only"):
        last_game_params['over_under'] = 1
        last_game_params['over_under_result'] = "under"
    else:
        last_game_params['over_under'] = 1
        last_game_params['over_under_result'] = "over"

    if (data.POST.get('filter_last_game_points_scored') == "on"):
        last_game_params['team_actual_total'] = 1
        if (data.POST.get('filter_last_game_points_scored_min') == ""):
            last_game_params['team_actual_total_min'] = 0
        else:
            last_game_params['team_actual_total_min'] = data.POST.get('filter_last_game_points_scored_min')

        if (data.POST.get('filter_last_game_points_scored_max') == ""):
            last_game_params['team_actual_total_max'] = 1000
        else:
            last_game_params['team_actual_total_max'] = data.POST.get('filter_last_game_points_scored_max')
    else:
        last_game_params['team_actual_total'] = 0
        last_game_params['team_actual_total_min'] = 0
        last_game_params['team_actual_total_max'] = 0

    if (data.POST.get('filter_last_game_points_allowed') == "on"):
        last_game_params['opp_team_actual_total'] = 1
        if (data.POST.get('filter_last_game_points_allowed_min') == ""):
            last_game_params['opp_team_actual_total_min'] = 0
        else:
            last_game_params['opp_team_actual_total_min'] = data.POST.get('filter_last_game_points_allowed_min')

        if (data.POST.get('filter_last_game_points_allowed_max') == ""):
            last_game_params['opp_team_actual_total_max'] = 1000
        else:
            last_game_params['opp_team_actual_total_max'] = data.POST.get('filter_last_game_points_allowed_max')
    else:
        last_game_params['opp_team_actual_total'] = 0
        last_game_params['opp_team_actual_total_min'] = 0
        last_game_params['opp_team_actual_total_max'] = 0

    if (data.POST.get('filter_last_game_margin_of_victory') == "on"):
        last_game_params['margin_of_victory'] = 1
        if (data.POST.get('filter_last_game_margin_of_victory_min') == ""):
            last_game_params['filter_last_game_margin_of_victory_min'] = 0
        else:
            last_game_params['filter_last_game_margin_of_victory_min'] = data.POST.get(
                'filter_last_game_margin_of_victory_min')

        if (data.POST.get('filter_last_game_margin_of_victory_max') == ""):
            last_game_params['margin_of_victory_max'] = 1000
        else:
            last_game_params['margin_of_victory_max'] = data.POST.get('filter_last_game_margin_of_victory_max')
    else:
        last_game_params['margin_of_victory'] = 0
        last_game_params['margin_of_victory_min'] = 0
        last_game_params['margin_of_victory_max'] = 0

    if (data.POST.get('filter_last_game_opponent_record') == "on"):
        last_game_params['opp_record'] = 1
        if (data.POST.get('filter_last_game_opponent_win_pct_min') == ""):
            last_game_params['opp_record_min_win_pct'] = 0
        else:
            last_game_params['opp_record_min_win_pct'] = data.POST.get('filter_last_game_opponent_win_pct_min')

        if (data.POST.get('filter_last_game_opponent_win_pct_max') == ""):
            last_game_params['opp_record_max_win_pct'] = 1
        else:
            last_game_params['opp_record_max_win_pct'] = data.POST.get('filter_last_game_opponent_win_pct_max')
    else:
        last_game_params['opp_record'] = 0
        last_game_params['opp_record_min_win_pct'] = 0
        last_game_params['opp_record_max_win_pct'] = 0

    if (data.POST.get('filter_last_game_spread_difference') == "on"):
        last_game_params['spread_difference'] = 1
        if (data.POST.get('filter_last_game_spread_difference_min') == ""):
            last_game_params['spread_difference_min'] = 0
        else:
            last_game_params['spread_difference_min'] = data.POST.get('filter_last_game_spread_difference_min')

        if (data.POST.get('filter_last_game_spread_difference_max') == ""):
            last_game_params['spread_difference_max'] = 1000
        else:
            last_game_params['spread_difference_max'] = data.POST.get('filter_last_game_spread_difference_max')
    else:
        last_game_params['spread_difference'] = 0
        last_game_params['spread_difference_min'] = 0
        last_game_params['spread_difference_max'] = 0

    if (data.POST.get('filter_last_game_total_difference') == "on"):
        last_game_params['total_difference'] = 1
        if (data.POST.get('filter_last_game_total_difference_min') == ""):
            last_game_params['total_difference_min'] = 0
        else:
            last_game_params['total_difference_min'] = data.POST.get('filter_last_game_total_difference_min')

        if (data.POST.get('filter_last_game_total_difference_max') == ""):
            last_game_params['total_difference_max'] = 1000
        else:
            last_game_params['total_difference_max'] = data.POST.get('filter_last_game_total_difference_max')
    else:
        last_game_params['total_difference'] = 0
        last_game_params['total_difference_min'] = 0
        last_game_params['total_difference_max'] = 0

    for key, val in last_game_params.items():
        if val == 1:
            last_game_params['on'] = 1
            break

    last_game_tracking_params = {}
    last_game_tracking_params['on'] = 0

    if (data.POST.get('filter_last_game_distance_traveled') == "on"):
        last_game_tracking_params['dist_trav'] = 1
        if (data.POST.get('filter_last_game_distance_traveled_min') == ""):
            last_game_tracking_params['dist_trav_min'] = 0
        else:
            last_game_tracking_params['dist_trav_min'] = data.POST.get('filter_last_game_distance_traveled_min')

        if (data.POST.get('filter_last_game_distance_traveled_max') == ""):
            last_game_tracking_params['dist_trav_max'] = 1000
        else:
            last_game_tracking_params['dist_trav_max'] = data.POST.get('filter_last_game_distance_traveled_max')
    else:
        last_game_tracking_params['dist_trav'] = 0
        last_game_tracking_params['dist_trav_min'] = 0
        last_game_tracking_params['dist_trav_max'] = 0

    last_game_tracking_params['cfg_pct'] = 0
    last_game_tracking_params['cfg_pct_min'] = 0
    last_game_tracking_params['cfg_pct_max'] = 0

    global_data = set_params(additional_params, last_game_params, last_game_tracking_params, x_axis, y_axis)

    return global_data
