#Retrieve data about a team or team(s) that fit specified parameters
from scripts.query_builder import *
#import numpy as np
#import pandas as pd

class TeamLastNGamesBetting:
	
	def __init__(self, team_abbrev, game_num, num_games, additional_params, last_game_params, last_game_tracking_params):
		self.team_abbrev = team_abbrev
		self.game_num = game_num
		self.num_games = num_games
		self.additional_params = additional_params
		self.last_game_params = last_game_params
		self.last_game_tracking_params = last_game_tracking_params

	def last_N(self):
		
		eligible_game_id_list = get_game_id_list(self.team_abbrev, self.game_num, self.additional_params['season'])
		addtl_param_query = additional_param_builder(self.additional_params)

		if (self.additional_params['opp_record']):
			eligible_game_id_list = apply_record_filter(self.team_abbrev, eligible_game_id_list, self.additional_params['opp_record_min_win_pct'], self.additional_params['opp_record_max_win_pct'], "opp")
		
		if (self.last_game_params['on'] == 1):
			eligible_game_id_list = last_game_param_parser(self.last_game_params, eligible_game_id_list, self.team_abbrev, self.last_game_tracking_params)

		if (self.additional_params['coach_filter'] != "All"):
			eligible_game_id_list = apply_coach_filter(self.team_abbrev, eligible_game_id_list, self.additional_params['coach_filter'])

		if (self.additional_params['official_filter'] != "All"):
			eligible_game_id_list = apply_official_filter(self.team_abbrev, eligible_game_id_list, self.additional_params['official_filter'])

		if (len(eligible_game_id_list) == 0):
			return None

		if (eligible_game_id_list is None):
			return None
			
		last_N_query = "SELECT * FROM teams_betting.{0} WHERE game_id IN {1}{2}".format(self.team_abbrev, "(" + ','.join(map(str, eligible_game_id_list)) + ")", addtl_param_query)
		result_set = get_result_set(last_N_query)
		result_set = manipulate_result_set(result_set)
		#result_set = add_advanced_stats(result_set)
		index = []

		for x in range(0, len(result_set)):
			index.append(x)

		if len(result_set) == 0:
			return None

		result_set = pd.DataFrame(data=result_set, index=index, columns=result_set[0])
		pd.set_option('display.max_rows', 500)
		pd.set_option('display.max_columns', 100)
		return result_set


class AllTeamsLastNGamesBetting:
	def __init__(self, additional_params, last_game_params, last_game_tracking_params, start_date=None, end_date=None):
		self.start_date = start_date
		self.end_date = end_date
		self.additional_params = additional_params
		self.last_game_params = last_game_params
		self.last_game_tracking_params = last_game_tracking_params

	def last_N(self):
		try:
			result_set = get_allteams_games(self.start_date, self.end_date, self.additional_params, self.last_game_params, self.last_game_tracking_params)		
			result_set = manipulate_result_set(result_set)

			#result_set = add_advanced_stats(result_set)
			print("got result set")
		except Exception as e:
			print(e)
			return None
		index = []

		for x in range(0, len(result_set)):
			index.append(x)

		if len(result_set) == 0:
			return None

		result_set = pd.DataFrame(data=result_set, index=index, columns=result_set[0])
		#pd.set_option('display.max_rows', 1000)
		#pd.set_option('display.max_columns', 100)

		return result_set


